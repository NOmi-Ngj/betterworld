//
//  VideoRecorderManager.swift
//  IOS-Swift-VideoRecorder01
//
//  Created by Saad Ahmed on 29/03/2022.
//  Copyright © 2022 Pooya Hatami. All rights reserved.
//

import UIKit
import AVKit
import MobileCoreServices

class VideoRecorderManager:NSObject{
    
    static let shared = VideoRecorderManager()
    var viewController:UIViewController!
    var videoAndImageReview = UIImagePickerController()
    var videoURL: URL?
    var videoData: Data?
    var didSuccessfullyRecordedVideo:((Bool)->Void)?
    
    
    func openCamera(){
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
            print("Camera Available")
            
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .camera
            imagePicker.videoMaximumDuration = 5
            imagePicker.mediaTypes = [kUTTypeMovie as String]
            imagePicker.allowsEditing = true
            
            self.viewController.present(imagePicker, animated: true, completion: nil)
        } else {
            print("Camera UnAvaialable")
        }
    }
}

extension VideoRecorderManager:UIImagePickerControllerDelegate , UINavigationControllerDelegate{
 
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        debugPrint("cancel")
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        // MARK: Todo
            self.viewController.dismiss(animated: true, completion: nil)
            
        guard
                let mediaType = info[UIImagePickerController.InfoKey.mediaType] as? String,
                mediaType == (kUTTypeMovie as String),
                let url = info[UIImagePickerController.InfoKey.mediaURL] as? URL,
                UIVideoAtPathIsCompatibleWithSavedPhotosAlbum(url.path)
                else { return }
                do{
                    debugPrint(url)
                    self.videoURL = url
                    let videoData = try? Data.init(contentsOf: url)
                    self.videoData = videoData
                    self.didSuccessfullyRecordedVideo?(true)
                }catch (let error){
                    print(error.localizedDescription)
                    debugPrint(error.localizedDescription)
                    self.didSuccessfullyRecordedVideo?(false)
                }
    }
    
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [String : Any]) {
  
        // Handle a movie capture
//        UISaveVideoAtPathToSavedPhotosAlbum(url.path,self,#selector(video(_:didFinishSavingWithError:contextInfo:)),nil)
    }
    
    @objc func video(_ videoPath: String, didFinishSavingWithError error: Error?, contextInfo info: AnyObject) {
        let title = (error == nil) ? "Success" : "Error"
        let message = (error == nil) ? "Video was saved" : "Video failed to save"
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.cancel, handler: nil))
        // MARK: Todo
        self.viewController.present(alert, animated: true, completion: nil)
    }
  
     func openVideoGalleryPicker() {
        videoAndImageReview.sourceType = .savedPhotosAlbum
        videoAndImageReview.delegate = self
        videoAndImageReview.mediaTypes = ["public.movie"]
        // MARK: Todo
        self.viewController.present(videoAndImageReview, animated: true, completion: nil)
    }
    
    func videoAndImageReview(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        videoURL = info[UIImagePickerController.InfoKey.mediaType.rawValue] as? URL
        print("videoURL:\(String(describing: videoURL))")
        // MARK: Todo
        self.viewController.dismiss(animated: true, completion: nil)
    }

}
