//
//  CustomView.swift
//  Matajer
//
//  Created by Nouman Gul on 19/10/2020.
//

import UIKit

@IBDesignable
class CardView: UIView {
    
    @IBInspectable var borderColor: UIColor = UIColor.white {
        didSet {
            self.layer.borderColor = borderColor.cgColor
        }
    }
    @IBInspectable var borderWidth: CGFloat = 2.0 {
        didSet {
            self.layer.borderWidth = borderWidth
        }
    }
    @IBInspectable var cornerRadius: CGFloat = 0.0 {
        didSet {
            self.layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable var isCircle: Bool{
        get{
            return false
        }
        set (newValue) {
            if newValue {
                DispatchQueue.main.async {
                    self.layer.masksToBounds = false
                    self.layer.cornerRadius = self.frame.height/2
                    self.clipsToBounds = true
                }
            }
        }
    }
    
    @IBInspectable
    var shadowRadius: CGFloat {
        get {
            return layer.shadowRadius
        }
        set {
            layer.shadowRadius = newValue
        }
    }
    
    @IBInspectable
    var shadowOpacity: Float {
        get {
            return layer.shadowOpacity
        }
        set {
            layer.shadowOpacity = newValue
        }
    }
    
    @IBInspectable
    var shadowOffset: CGSize {
        get {
            return layer.shadowOffset
        }
        set {
            layer.shadowOffset = newValue
        }
    }
    
    @IBInspectable
    var shadowColor: UIColor? {
        get {
            if let color = layer.shadowColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                
                layer.shadowColor = color.cgColor
            } else {
                layer.shadowColor = nil
            }
        }
    }
    
    private var shapeLayer : CAShapeLayer!
    
    func updown(color:UIColor){
        
        shapeLayer = CAShapeLayer()
        shapeLayer.strokeColor = color.cgColor
        shapeLayer.bounds = self.frame
        shapeLayer.position = self.center
        shapeLayer.path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: [.topLeft , .topRight , .bottomLeft, .bottomRight], cornerRadii: CGSize(width: self.frame.width/2, height: self.frame.width/2)).cgPath
        //Here I'm masking the textView's layer with rectShape layer
        self.layer.mask = shapeLayer
    }

}
