//
//  MyTabBarCtrl.swift
//  BWorld
//
//  Created by Invision-102 on 2/9/22.
//

import UIKit

class MyTabBarCtrl: UITabBarController, UITabBarControllerDelegate {
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }

    static var midButtonIndex = 2
    var middleBtn:UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        self.selectedIndex = MyTabBarCtrl.midButtonIndex
        setupMiddleButton()
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(swiped))
        swipeRight.direction = UISwipeGestureRecognizer.Direction.right
        self.view.addGestureRecognizer(swipeRight)

        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(swiped))
        swipeLeft.direction = UISwipeGestureRecognizer.Direction.left
        self.view.addGestureRecognizer(swipeLeft)
        
    }
    
    @objc func swiped(_ gesture: UISwipeGestureRecognizer) {
        let tabs = self.children.count
        if gesture.direction == .left {
            if (self.selectedIndex) < tabs { // set your total tabs here
                self.selectedIndex += 1
            }
        } else if gesture.direction == .right {
            if (self.selectedIndex) > 0 {
                self.selectedIndex -= 1
            }else{
                debugPrint("show side menu")
                if let nav = self.selectedViewController as? UINavigationController{
                    if nav.presentedViewController is ViewController{
                        self.sideMenuController?.showLeftView()
                    }else{
                        nav.popToRootViewController(animated: true)
                    }
                }
            }
        }
    }
    
    // TabBarButton – Setup Middle Button
     func setupMiddleButton() {

         middleBtn = UIButton(frame: CGRect(x: (self.view.bounds.width / 2)-25, y: -20, width: 50, height: 50))
         
         middleBtn.setImage(UIImage(named: "home_icon"), for: .normal)
         middleBtn.setImage(UIImage(named: "home_icon_selected"), for: .selected)
         
         middleBtn.setBackgroundImage(UIImage(named: "AddIcon"), for: UIControl.State())
         middleBtn.setBackgroundImage(UIImage(named: "AddIcon"), for: UIControl.State.highlighted)
         //STYLE THE BUTTON YOUR OWN WAY
//         middleBtn.setIcon(icon: , iconSize: 20.0, color: UIColor.white, backgroundColor: UIColor.white, forState: .normal)
//         middleBtn.applyGradient(colors: colorBlueDark.cgColor,colorBlueLight.cgColor])
         
         //add to the tabbar and add click event
         self.tabBar.addSubview(middleBtn)
         middleBtn.addTarget(self, action: #selector(self.menuButtonAction), for: .touchUpInside)

         self.view.layoutIfNeeded()
     }

     // Menu Button Touch Action
     @objc func menuButtonAction(sender: UIButton) {
         sender.isSelected = true
         self.selectedIndex = MyTabBarCtrl.midButtonIndex   //to select the middle tab. use "1" if you have only 3 tabs.
     }
}

extension MyTabBarCtrl  {
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {

        guard let fromView = selectedViewController?.view, let toView = viewController.view else {
          return false // Make sure you want this as false
        }

        if fromView != toView {
          UIView.transition(from: fromView, to: toView, duration: 0.5, options: [.transitionCrossDissolve], completion: nil)
        }

        return true
    }
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {

        if let index = self.tabBar.items?.index(of: item), let subView = tabBar.subviews[index].subviews.first as? UIImageView{
            if self.selectedIndex == MyTabBarCtrl.midButtonIndex{
                middleBtn.isSelected = true
            }else{
                middleBtn.isSelected = false
            }
            self.performSpringAnimation(imgView: subView)
        }
    }
    
    //func to perform spring animation on imageview
    func performSpringAnimation(imgView: UIImageView) {

        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: .curveEaseInOut, animations: {

            imgView.transform = CGAffineTransform.init(scaleX: 1.4, y: 1.4)

            //reducing the size
            UIView.animate(withDuration: 0.5, delay: 0.2, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: .curveEaseInOut, animations: {
                imgView.transform = CGAffineTransform.init(scaleX: 1, y: 1)
            }) { (flag) in
            }
        }) { (flag) in

        }
    }
}
