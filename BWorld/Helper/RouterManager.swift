//
//  RouterManager.swift
//  BWorld
//
//  Created by Kassim Denim on 17/03/2022.
//

import UIKit


class RouteManager: NSObject {
    
    static var shared = RouteManager()
    
    private override init() {}
    
    var delegate:AppDelegate?{
        return UIApplication.shared.delegate as? AppDelegate
    }
    
    var window:UIWindow?? {
        return UIApplication.shared.delegate?.window
    }
   
    func showHome(){
        let tabBar = DrawerViewController.getVC(.structure)
//        let tabBar = CreateNewProjectViewController.getVC(.mediaPlay)
        window??.backgroundColor = .white
        window??.rootViewController = tabBar
    }
    
    func showLogin(){
        clear(window: window)
        let authNC = LoginViewController.getVC(.auth)
        let nv = UINavigationController(rootViewController: authNC)
        nv.isNavigationBarHidden = true
        window??.rootViewController = nv
    }
    
    func clear(window: UIWindow??) {
        window??.subviews.forEach { $0.removeFromSuperview() }
    }
    func showCustomSplash() {
           clear(window: window)
           let customSplashVC = UIStoryboard.init(name: "CustomLaunchStoryboard", bundle: nil).instantiateViewController(withIdentifier: "CustomLaunchViewController")
           window??.rootViewController = customSplashVC
    }
    
    func showHomeAnimated(){
//        let tabBar:RaisedTabBarController = UIStoryboard(storyboard: .main).instantiateViewController()
        let tabBar:DrawerViewController = DrawerViewController.getVC(.structure)
        setRootViewController(tabBar)
    }
    
    func setRootViewController(_ vc: UIViewController, animated: Bool = true) {
        guard animated, let window = self.window else {
            self.window??.rootViewController = vc
            self.window??.makeKeyAndVisible()
            return
        }

        window?.rootViewController = vc
        window?.makeKeyAndVisible()
        UIView.transition(with: window!,
                          duration: 0.3,
                          options: .transitionCrossDissolve,
                          animations: nil,
                          completion: nil)
    }
}
