//
//  AppDefaults.swift
//  BWorld
//
//  Created by Kassim Denim on 17/03/2022.
//

import Foundation


class AppDefaults {
    
    public static let defaults = UserDefaults.init()
    
    // MARK: - CLEAR ALL USER DEFAULTS
    
    public static func clearUserDefaults(){
        let dictionary = AppDefaults.defaults.dictionaryRepresentation()
        dictionary.keys.forEach { key in
            
            //FIXME: I omit deleting of persisted language by user.
            if (key == "AppleLanguages" ||
                key == "hasUserSelectedAnyLanguage" ||
                key == "FCMToken" ||
                key == "pushToken" ||
                key == "deviceToken" ||
                key == "deviceTokenData" ||
                key == "loginEmail" ||
                key == "isRememberMe" ||
                key == "loginPassword") {
                //Don't Remove These keys
            }else {
                defaults.removeObject(forKey: key)
            }
        }
    }
    
    //MARK: - USER OBJECT
    
//
    public static var rememberMe: Bool? {
        get{
            if let data = AppDefaults.defaults.data(forKey: "isRememberMe") {
                let decoder = JSONDecoder()
                do{
                    let decoded = try decoder.decode(Bool.self, from: data)
                    return decoded
                }catch{
                    return false
                }
            }
            return false
        }
        set{
            let encoder = JSONEncoder()
            do {
                let jsonData = try encoder.encode(newValue)
                AppDefaults.defaults.set(jsonData, forKey: "isRememberMe")

            }catch{
                debugPrint("CurrentUser not save")
            }
        }
    }
    
    
    
    public static var currentUser: UserData? {
        get{
            if let data = AppDefaults.defaults.data(forKey: "CurrentUser") {
                let decoder = JSONDecoder()
                do{
                    let decoded = try decoder.decode(UserData.self, from: data)
                    return decoded
                }catch{
                    return nil
                }
            }
            return nil
        }
        set{
            let encoder = JSONEncoder()
            do {
                let jsonData = try encoder.encode(newValue)
                AppDefaults.defaults.set(jsonData, forKey: "CurrentUser")

            }catch{
                debugPrint("CurrentUser not save")
            }
        }
    }
}
