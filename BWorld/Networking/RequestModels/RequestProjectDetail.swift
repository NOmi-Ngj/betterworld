//
//  ProjectDetailRequest.swift
//  BWorld
//
//  Created by Kassim Denim on 15/02/2022.
//

import Foundation

// MARK: - RequestProjectDetail
struct RequestProjectDetail: Codable {
    var projectid: String?
    
    enum CodingKeys: String, CodingKey {
        case projectid
    }
    
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        
        if projectid != nil{
            dictionary[CodingKeys.projectid.rawValue] = projectid
        }
        
        return dictionary
    }
}
