//
//  RequestRegisterUser.swift
//  BWorld
//
//  Created by Kassim Denim on 15/03/2022.
//

import UIKit

// MARK: - RequestRegisterUser
struct RequestRegisterUser: Codable {
    var email: String
    var password: String
    var firstname: String
    var lastname: String
    var phone: String
    var gender: String
    var country: String
    var address: String
   
    init(email: String, password: String, firstname: String, lastname: String, phone: String, gender: String, country: String, address: String){
        
        self.email = email
        self.password = password
        self.firstname = firstname
        self.lastname = lastname
        self.phone = phone
        self.gender = gender
        self.country = country
        self.address = address
    }

    
    enum CodingKeys: String, CodingKey {
        case email = "email"
        case password = "password"
        case firstname = "firstname"
        case lastname = "lastname"
        case phone = "phone"
        case gender = "gender"
        case country = "country"
        case address = "address"
    }
    
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        dictionary[CodingKeys.email.rawValue] = email
        dictionary[CodingKeys.password.rawValue] = password
        dictionary[CodingKeys.firstname.rawValue] = firstname
        dictionary[CodingKeys.lastname.rawValue] = lastname
        dictionary[CodingKeys.phone.rawValue] = phone
        dictionary[CodingKeys.gender.rawValue] = gender
        dictionary[CodingKeys.country.rawValue] = country
        dictionary[CodingKeys.address.rawValue] = address
        
        return dictionary
    }
}
