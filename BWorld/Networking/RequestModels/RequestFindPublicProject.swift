//
//  FindPublicProjectRequest.swift
//  BWorld
//
//  Created by Kassim Denim on 15/02/2022.
//

import Foundation

// MARK: - RequestFindPublicProject
struct RequestFindPublicProject: Codable {
    var page, size, pollstatus: Int?
    var email: String?
    
    enum CodingKeys: String, CodingKey {
        case page,size,email,pollstatus
    }
    
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        
        if page != nil{
            dictionary[CodingKeys.page.rawValue] = page
        }
        
        if size != nil{
            dictionary[CodingKeys.size.rawValue] = size
        }
        
        if email != nil{
            dictionary[CodingKeys.email.rawValue] = email
        }
        
        if pollstatus != nil{
            dictionary[CodingKeys.pollstatus.rawValue] = pollstatus
        }
        
        return dictionary
    }
}
