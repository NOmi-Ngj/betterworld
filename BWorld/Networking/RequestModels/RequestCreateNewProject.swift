//
//  RequestCreateNewProject.swift
//  BWorld
//
//  Created by Saad Ahmed on 24/03/2022.
//

import Foundation


// MARK: - RequestCreateNewProject
struct RequestCreateNewProject: Codable {
    var email: String
    var projecttitle: String
    var projectdescription: String
    var totaltrees: String
    var lat: String
    var lon: String
    var location: String

    enum CodingKeys: String, CodingKey {
        case email = "email"
        case projecttitle = "projecttitle"
        case projectdescription = "projectdescription"
        case totaltrees = "totaltrees"
        case lat = "lat"
        case lon = "lon"
        case location = "location"
    }
    
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        dictionary[CodingKeys.email.rawValue] = email
        dictionary[CodingKeys.projecttitle.rawValue] = projecttitle
        dictionary[CodingKeys.projectdescription.rawValue] = projectdescription
        dictionary[CodingKeys.totaltrees.rawValue] = totaltrees
        dictionary[CodingKeys.lat.rawValue] = lat
        dictionary[CodingKeys.lon.rawValue] = lon
        dictionary[CodingKeys.location.rawValue] = location
        return dictionary
    }
    
}
