//
//  RequestUploadProjectVideo.swift
//  BWorld
//
//  Created by Saad Ahmed on 28/03/2022.
//


import Foundation
import Moya

// MARK: - RequestCreateNewProject
struct RequestUploadProjectVideo: Codable {
    var email: String
    var videoformat: String
    var videoData: Data
    var lat : Double
    var lon : Double
    var projectid : String

//    enum CodingKeys: String, CodingKey {
//        case email = "email"
//        case projecid = "projecid"
//        case videoformat = "videoformat"
//        case videoData = "treeproject"
//    }
    
//    func toDictionary() -> [String:Any]
//    {
//        var dictionary = [String:Any]()
////        dictionary[CodingKeys.email.rawValue] = email
////        dictionary[CodingKeys.projecid.rawValue] = projecid
////        dictionary[CodingKeys.videoformat.rawValue] = videoformat
//
//        return dictionary
//    }
    
    enum CodingKeys: String, CodingKey {
        case email = "email"
//        case projecid = "projecid"
        case videoformat = "videoformat"
        case videoData = "treeproject"
        case lat = "lat"
        case lon = "lon"
        case projectid = "projectid"
    }
    
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        dictionary[CodingKeys.lat.rawValue] = 12.123
        dictionary[CodingKeys.lon.rawValue] = 12.2345
        dictionary[CodingKeys.projectid.rawValue] = projectid
        
        return dictionary
    }
//    func toMultipartDictionary() -> [MultipartFormData]
//    {
//        var MultipartData:[Moya.MultipartFormData] = []
//
//        MultipartData.append(MultipartFormData(provider: .data(self.videoData), name: CodingKeys.videoData.rawValue, fileName: "\(self.projecid)/video.mp4", mimeType: "video/mp4"))
//
//        if let email = email.data(using: String.Encoding.utf8){
//            MultipartData.append(Moya.MultipartFormData(provider: .data(email), name: CodingKeys.email.rawValue))
//        }
//
//        if let projecid = projecid.data(using: String.Encoding.utf8){
//            MultipartData.append(Moya.MultipartFormData(provider: .data(projecid), name: CodingKeys.videoformat.rawValue))
//        }
//
//        if let videoformat = videoformat.data(using: String.Encoding.utf8){
//            MultipartData.append(Moya.MultipartFormData(provider: .data(videoformat), name: CodingKeys.videoformat.rawValue))
//        }
//
//        return MultipartData
//    }
    
    func toMultipartDictionary() -> [MultipartFormData]
        {
            var multipartData:[Moya.MultipartFormData] = []
            multipartData.append(Moya.MultipartFormData(provider: .data(self.videoData), name: CodingKeys.videoData.rawValue))

            if let email = email.data(using: .utf8){
                multipartData.append(Moya.MultipartFormData(provider: .data(email), name: CodingKeys.email.rawValue))
            }
            
            if let projectid = projectid.data(using: .utf8){
                multipartData.append(Moya.MultipartFormData(provider: .data(projectid), name: CodingKeys.videoformat.rawValue))
            }
            
            if let videoformat = videoformat.data(using: .utf8){
                multipartData.append(Moya.MultipartFormData(provider: .data(videoformat), name: CodingKeys.videoformat.rawValue))
            }
            return multipartData
        }
    
}
