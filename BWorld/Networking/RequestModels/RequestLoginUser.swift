//
//  RequestLoginUser.swift
//  BWorld
//
//  Created by Kassim Denim on 16/03/2022.
//

import UIKit

struct RequestLoginUser: Codable {
    var email: String
    var password: String

    enum CodingKeys: String, CodingKey {
        case email = "email"
        case password = "password"
    }
    
    init(email: String, password: String){
        
        self.email = email
        self.password = password
    }


    
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        dictionary[CodingKeys.email.rawValue] = email
        dictionary[CodingKeys.password.rawValue] = password
        return dictionary
    }
}
