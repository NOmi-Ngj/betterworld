//
//  Services.swift
//  Vease
//
//  Created by Invision on 02/08/2019.
//  Copyright © 2019 Invision. All rights reserved.
//

import Foundation
import Moya

enum Services {
    case getUserProfile
    case registerUser(data:RequestRegisterUser)
    case loginUser(data:RequestLoginUser)
    case getMyProjects(data:RequestFindPublicProject)
    case getPublicProject(data:RequestFindPublicProject)
    case createNewProject(data: RequestCreateNewProject)
    case uploadProjectVideo(data:RequestUploadProjectVideo)
    case getAirFieldsRewards
}

extension Services: TargetType {
    
    var baseURL: URL {
        
        switch self {
        default:
            let urlString = ApiRouter.getBaseURL()
            return URL.init(string: urlString)!
        }
    }
    
    var path: String {
        switch self {
        case .getUserProfile:
            return "updateUser"
        case .getMyProjects:
            return "findmyplantedtrees"
        case .getPublicProject:
            return "findplantedtreesatpollstatus"
        case .registerUser:
            return "Register"
        case .loginUser:
            return "login"
        case .createNewProject:
            return "newtreeproject"
        case .uploadProjectVideo(let data):
            return "planttreeaws"//"planttreeaws?lat=\(String(describing: LocationManager.sharedInstance.currentLocation!.currentLat!))&lon=\(String(describing: LocationManager.sharedInstance.currentLocation!.currentLng!))&projectid=\(data.projecid)"
        case .getAirFieldsRewards:
            return"getairfields"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .getUserProfile:
            return .get
        default:
            return .post
        }
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        switch self {
        case .getUserProfile:
            return .requestPlain
        case .getMyProjects(let data):
            return .requestParameters(parameters: data.toDictionary(), encoding: URLEncoding.queryString)
        case .getPublicProject(let data):
            return .requestParameters(parameters: data.toDictionary(), encoding: URLEncoding.queryString)
        case .registerUser(let data):
            return .requestParameters(parameters: data.toDictionary(), encoding: JSONEncoding.default)
        case .loginUser( let data):
            return .requestParameters(parameters: data.toDictionary(), encoding: JSONEncoding.default)
        case .createNewProject( let data):
            return .requestParameters(parameters: data.toDictionary(), encoding: JSONEncoding.default)
        case .uploadProjectVideo(let data):
            return .uploadCompositeMultipart(data.toMultipartDictionary(), urlParameters: data.toDictionary())
        case .getAirFieldsRewards: //AppDefaults.currentUser?.email ?? ""   anonumous.developer@gmail.com
            return .requestParameters(parameters: ["email": "anonumous.developer@gmail.com"], encoding: URLEncoding.queryString)
        }
    }
    
    var headers: [String: String]? {
        switch self {
        default:
            return [:]
        }
    }
    
    var validationType: ValidationType {
        var combined = [Int]()
        combined.append(contentsOf: 200..<299)
        combined.append(contentsOf: 400..<500)
        combined.append(contentsOf: 500..<599)
        return .customCodes(combined)
    }
    
}
