//
//  NetworkManager.swift
//  VenueVision
//
//  Created by Invision040-Raza on 17/09/2019.
//  Copyright © 2019 Muhammad Raza. All rights reserved.
//

import Foundation
import Moya

class NetworkManager {
    
    // MARK: - GET USER PROFILE
    
    class func getUserProfile(completion: @escaping (APIResult<Bool>)->Void) {
        Provider.backgroundServices.request(.getUserProfile) { (result) in
            do {
                let response: GenericAPIModel = try result.decoded()
                if response.success ?? false {
                    completion(.success(response.success!))
                } else {
                    completion(.failure(response.message?.first ?? "Something went wrong!"))
                }
            } catch {
                completion(.failure(error.customLocalizedError))
            }
        }
    }
}
