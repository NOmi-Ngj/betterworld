//
//  Provider.swift
//  Vease
//
//  Created by Invision on 02/08/2019.
//  Copyright © 2019 Invision. All rights reserved.
//

import Foundation
import Moya
import Alamofire

struct Provider {
    static let services = MoyaProvider<Services>(plugins: [Plugin.networkPlugin, Plugin.loggerPlugin])
    static let backgroundServices = MoyaProvider<Services>(plugins: [Plugin.loggerPlugin])
    
    static func getMoyaProvider(shouldShowActivityIndicator: Bool = true) -> MoyaProvider<Services> {
        return shouldShowActivityIndicator ? Provider.services : Provider.backgroundServices
    }
     
}

