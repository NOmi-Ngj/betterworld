//
//  Pluggin.swift
//  Vease
//
//  Created by Invision on 02/08/2019.
//  Copyright © 2019 Invision. All rights reserved.
//

import Foundation
import Moya
import NVActivityIndicatorView
struct Plugin {
    
    private init() {}
    
    static let networkPlugin = NetworkActivityPlugin(networkActivityClosure: { (changeType, _) in
        switch changeType {
        case .began:
            debugPrint("🌍 -->> Network Call Started... Data & Time -->> \(Date()) <<--")
            UIApplication.startActivityIndicator()
        case .ended:
            UIApplication.stopActivityIndicator()
            debugPrint("🌍 -->> Network Call Ended... Data & Time -->> \(Date()) <<--")
        }
    })
    
    static let loggerPlugin = NetworkLoggerPlugin()
}

extension UIApplication{
    //Show hide loader.
    class func startActivityIndicator(with message: String? = "") {
        DispatchQueue.main.async {
            let topView = UIApplication.topViewController()
            let hud = NVActivityIndicatorView.init(frame: CGRect.init(x: topView.view.frame.midX-100, y: topView.view.frame.midY-100, width: 200, height: 200), type: .ballClipRotatePulse, color: .gray, padding: 20)
            hud.tag = 999
            if let view = self.shared.keyWindow {
                hud.startAnimating()
                view.addSubview(hud)
            }
        }
    }

    class func stopActivityIndicator(){
        DispatchQueue.main.async {
            self.shared.keyWindow?.subviews.forEach({ (view) in
                if view.tag == 999, let hud = view as? NVActivityIndicatorView {
//                    hud.stopAnimating()
                    hud.removeFromSuperview()
                }
            })
        }
    }
}
