//
//  ResponseGetProjects.swift
//  BWorld
//
//  Created by Kassim Denim on 15/02/2022.
//

import Foundation


struct ResponseCreateNewProject:Codable{
    var status_code:Int?
    var data:ResponseProjectDetail?
    var message:String?
}

// MARK: - ResponseResponseGetProjects
struct ResponseResponseGetProjects: Codable {
    let docs: [ResponseProjectDetail]?
    let totalDocs, offset, limit, totalPages: Int?
    let page, pagingCounter: Int?
    let hasPrevPage, hasNextPage: Bool?
    let prevPage, nextPage: Int?
    }

// MARK: - Doc
struct ResponseProjectDetail: Codable {
    let id, email, projectdate, projecttitle: String?
    let projectdescription: String?
    let thumbnail1, thumbnail2, thumbnail3: String?
    let videourl: String?
    let status:Int?
    let votesup: Int?
    let votesdown: Int?
    let requiredvotes: Int?
    //    let country, countrycode, city, address: String?
//    let totaltrees, requiredvotes, votesup, votesdown: Int?
//    let pollstatus, air, status, typeid: Int?
//    let v: Int?
//    let location: Location?
    
    func returnPollingCount()->String{
        let finalnum = (votesup ?? 0) + (votesdown ?? 0)
        return "\(finalnum)/\(self.requiredvotes ?? 0)"
    }
    

    func returnURlThumbnails()->[URL]{
        var urls:[URL] = []
        if let thumbnail1 = thumbnail1, let url = URL(string:thumbnail1) {
            urls.append(url)
        }
        if let thumbnail2 = thumbnail2, let url = URL(string:thumbnail2) {
            urls.append(url)
        }
        if let thumbnail3 = thumbnail3, let url = URL(string:thumbnail3) {
            urls.append(url)
        }
        return urls
    }
    
    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case email, projectdate, projecttitle, projectdescription
        case status
        case votesup
        case votesdown
        case requiredvotes
//        case totaltrees, requiredvotes, votesup, votesdown, pollstatus, air, status, typeid
        case thumbnail1, thumbnail2, thumbnail3, videourl
//        case country, countrycode, city, address, location
//        case v = "__v"
    }
    
    init(from decorder:Decoder) throws{
        let values = try decorder.container(keyedBy: CodingKeys.self)
        
        
        
        id = try values.decodeIfPresent(String.self, forKey: .id)
        email = try values.decodeIfPresent(String.self, forKey: .email)
        projectdate = try values.decodeIfPresent(String.self, forKey: .projectdate)
        projecttitle = try values.decodeIfPresent(String.self, forKey: .projecttitle)
        projectdescription = try values.decodeIfPresent(String.self, forKey: .projectdescription)
        thumbnail1 = try values.decodeIfPresent(String.self, forKey: .thumbnail1)
        thumbnail2 = try values.decodeIfPresent(String.self, forKey: .thumbnail2)
        thumbnail3 = try values.decodeIfPresent(String.self, forKey: .thumbnail3)
        videourl = try values.decodeIfPresent(String.self, forKey: .videourl)
        status = try values.decodeIfPresent(Int.self, forKey: .status)
        votesup = try values.decodeIfPresent(Int.self, forKey: .votesup)
        votesdown = try values.decodeIfPresent(Int.self, forKey: .votesdown)
        if let requiredvotes = try? values.decodeIfPresent(Double.self, forKey: .requiredvotes){
            self.requiredvotes = Int(requiredvotes ?? 0)
        }else if let requiredvotes = try? values.decodeIfPresent(String.self, forKey: .requiredvotes){
            self.requiredvotes = Int(requiredvotes)
        }else{
            let requiredvotes = try? values.decodeIfPresent(Int.self, forKey: .requiredvotes)
            self.requiredvotes = requiredvotes
        }
        
    }
}

// MARK: - Location
struct ProjectLocation: Codable {
    let id, type: String?
    let coordinates: [Double]?

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case type, coordinates
    }
}
