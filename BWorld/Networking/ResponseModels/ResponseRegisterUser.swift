//
//  ResponseRegisterUser.swift
//  BWorld
//
//  Created by Kassim Denim on 16/03/2022.
//

import Foundation



// MARK: - Response
struct ResponseRegisterUser: Codable {
    let statusCode: Int
    let data: UserData?
    let message: String?

    enum CodingKeys: String, CodingKey {
        case statusCode = "status_code"
        case data = "data"
        case message = "message"
    }
}

// MARK: - DataClass
struct UserData: Codable {
    //login and register response values & keys
    let id: String
    let email: String
    let password: String
    let firstname: String
    let lastname: String
    let phone: String
    let gender: String
    let country: String
    let address: String
    let status: Int
    let picurl: String
    
    // login response additional values & keys
    let accounttypeid: Int?
    let state: Int?
    let mailkey: String?
    let walletaddress: String?

       enum CodingKeys: String, CodingKey {
           case accounttypeid = "accounttypeid"
           case state = "state"
           case mailkey = "mailkey"
           case walletaddress = "walletaddress"
           case id = "_id"
           case email = "email"
           case password = "password"
           case firstname = "firstname"
           case lastname = "lastname"
           case phone = "phone"
           case gender = "gender"
           case country = "country"
           case address = "address"
           case status = "status"
           case picurl = "picurl"
       }
}
