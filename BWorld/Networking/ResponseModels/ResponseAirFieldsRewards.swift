//
//  ResponseAirFieldsRewards.swift
//  BWorld
//
//  Created by Saad Ahmed on 30/03/2022.
//



import Foundation


struct ResponseAirFieldsRewards:Codable{
    var status_code:Int?
    var data:[ResponseAirFieldRewardsDetails]?
    var message:String?
}



// MARK: - Doc
struct ResponseAirFieldRewardsDetails: Codable {
    let id, projectid, email, project: String?
    let description: String?
    let air_rewarded:Double?
    let air_transfered: Double?
    let air_rewarded_last: Double?
    let air_transfered_last: Double?
    let rewardedid_last, transferedid_last, air_icon: String?
   
    func returnIcon()->[URL]{
        var urls:[URL] = []
        if let air_icon = air_icon, let url = URL(string:air_icon ?? "") {
            urls.append(url)
        }
        return urls
    }
    
    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case email, projectid, project, description
        case air_rewarded
        case air_transfered
        case air_rewarded_last
        case air_transfered_last
        case rewardedid_last, transferedid_last, air_icon
    }
    
    init(from decorder:Decoder) throws{
        let values = try decorder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(String.self, forKey: .id)
        projectid = try values.decodeIfPresent(String.self, forKey: .projectid)
        email = try values.decodeIfPresent(String.self, forKey: .email)
        project = try values.decodeIfPresent(String.self, forKey: .project)
        description = try values.decodeIfPresent(String.self, forKey: .description)
        air_rewarded = try values.decodeIfPresent(Double.self, forKey: .air_rewarded)
        air_transfered = try values.decodeIfPresent(Double.self, forKey: .air_transfered)
        air_rewarded_last = try values.decodeIfPresent(Double.self, forKey: .air_rewarded_last)
        air_transfered_last = try values.decodeIfPresent(Double.self, forKey: .air_transfered_last)
        rewardedid_last = try values.decodeIfPresent(String.self, forKey: .rewardedid_last)
        transferedid_last = try values.decodeIfPresent(String.self, forKey: .transferedid_last)
        air_icon = try values.decodeIfPresent(String.self, forKey: .air_icon)
    }
}


