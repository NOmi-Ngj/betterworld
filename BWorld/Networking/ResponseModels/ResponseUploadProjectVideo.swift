//
//  ResponseUploadProjectVideo.swift
//  BWorld
//
//  Created by Saad Ahmed on 29/03/2022.
//

import Foundation

struct ResponseUploadProjectVideo:Codable{
    var status_code:Int?
    var data:String?
    var message:String?
}
