//
//  ProjectsNetworking.swift
//  BWorld
//  Created by Kassim Denim on 19/02/2022.

import Foundation
extension NetworkManager {
    
    class func getMyProjects(data: RequestFindPublicProject, completion: @escaping (APIResult<ResponseResponseGetProjects>)->Void) {
        Provider.services.request(.getMyProjects(data: data)) { (result) in
            do {
                let response: ResponseResponseGetProjects = try result.decoded()
                completion(.success(response))
            } catch {
                completion(.failure(error.customLocalizedError))
            }
        }
    }
    
    class func uploadProjectVideo(data: RequestUploadProjectVideo, completion: @escaping (APIResult<ResponseUploadProjectVideo>)->Void){
        Provider.services.request(.uploadProjectVideo(data: data)) { (result) in
            do {
                debugPrint(result)
//                debugPrint(String(data: result, encoding: .utf8))
                let response: ResponseUploadProjectVideo = try result.decoded()
                completion(.success(response))
            } catch {
                completion(.failure(error.customLocalizedError))
            }
        }
    }
    
    class func getPublicProjects(data: RequestFindPublicProject, completion: @escaping (APIResult<ResponseResponseGetProjects>)->Void) {
        Provider.services.request(.getPublicProject(data: data)) { (result) in
            do {
                let response: ResponseResponseGetProjects = try result.decoded()
                completion(.success(response))
            } catch {
                completion(.failure(error.customLocalizedError))
            }
        }
    }
    
    class func createNewProject(data: RequestCreateNewProject, completion: @escaping (APIResult<ResponseCreateNewProject>)->Void) {
        Provider.services.request(.createNewProject(data: data)) { (result) in
            do {
                let response: ResponseCreateNewProject = try result.decoded()
                completion(.success(response))
            } catch {
                completion(.failure(error.customLocalizedError))
            }
        }
    }

}
