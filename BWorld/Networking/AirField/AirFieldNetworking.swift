//
//  AirFieldNetworking.swift
//  BWorld
//
//  Created by Saad Ahmed on 30/03/2022.
//


import Foundation
extension NetworkManager {
    
    class func getMyAirFieldsRewards(completion: @escaping (APIResult<ResponseAirFieldsRewards>)->Void){
        Provider.services.request(.getAirFieldsRewards) { (result) in
            do {
                let response: ResponseAirFieldsRewards = try result.decoded()
                completion(.success(response))
            } catch {
                completion(.failure(error.customLocalizedError))
            }
        }
    }
}
