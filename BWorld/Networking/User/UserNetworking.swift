//
//  UserNetworkiong.swift
//  BWorld
//
//  Created by Kassim Denim on 16/03/2022.
//

import Foundation


extension NetworkManager {

    class func loginUser(data: RequestLoginUser, completion: @escaping (APIResult<ResponseRegisterUser>)->Void) {
        debugPrint("request Data: \(data.toDictionary())")
        Provider.services.request(.loginUser(data: data)) { (result) in
            do {
                let response: ResponseRegisterUser = try result.decoded()
                if response.statusCode == 200 || response.statusCode == 201{
                    completion(.success(response))
                }else{
                    completion(.failure(response.message ?? "Something went wrong"))
                }
            } catch {
                completion(.failure(error.customLocalizedError))
            }
        }
    }
    
    class func registerUser(data: RequestRegisterUser, completion: @escaping (APIResult<ResponseRegisterUser>)->Void) {
        debugPrint("request Data: \(data.toDictionary())")
        Provider.backgroundServices.request(.registerUser(data: data)) { (result) in
            do {
                let response: ResponseRegisterUser = try result.decoded()
                if response.statusCode == 200 || response.statusCode == 201{
                    completion(.success(response))
                }else{
                    completion(.failure(response.message ?? "Something went wrong"))
                }
            } catch {
                completion(.failure(error.customLocalizedError))
            }
        }
    }
    
}
