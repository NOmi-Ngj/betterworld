//
//  AppTextField.swift
//  BWorld
//
//  Created by Nouman Gul on 30/09/2020.
//

import UIKit

@IBDesignable
open class AppTextField: UITextField {
    // Programmatically: use the enum
    var fieldWithUnderline:Bool = false
    var padding = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 30)
    var classFont:FontClassType = .regular
    var classFontSize:CGFloat = 17.0
    
    
    open override func layoutSubviews() {
        super.layoutSubviews()
        setup()
    }
    
    func setup() {
        if fieldWithUnderline{
            let border = CALayer()
            let width = CGFloat(0.8)
            border.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            border.frame = CGRect(x: 0, y: self.frame.size.height - width, width:  self.frame.size.width - 10, height: self.frame.size.height)
            border.borderWidth = width
            self.layer.addSublayer(border)
            self.layer.masksToBounds = true
        }

    }

    // IB: use the adapter
    @IBInspectable var underline:Bool {
        get {
            return fieldWithUnderline
        }
        set( setUnderline) {
            fieldWithUnderline = setUnderline
        }
    }
    
    // IB: use the adapter
    @IBInspectable var cornerRadius: CGFloat = 10.0 {
        didSet {
            self.layer.cornerRadius = cornerRadius
        }
    }
    @IBInspectable var borderWidth: CGFloat = 0.0 {
        didSet {
            self.layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable public var placeholderColor: UIColor = .lightGray {
         didSet {
             let placeholderStr = placeholder ?? ""
             attributedPlaceholder = NSAttributedString(string: placeholderStr, attributes: [NSAttributedString.Key.foregroundColor: placeholderColor])
         }
     }

    @IBInspectable public override var placeholder: String? {
         didSet {
             let placeholderStr = placeholder ?? ""
             attributedPlaceholder = NSAttributedString(string: placeholderStr, attributes: [NSAttributedString.Key.foregroundColor: placeholderColor])
         }
     }
    
    @IBInspectable var borderColor: UIColor = .white {
        didSet {
            self.layer.borderColor = borderColor.cgColor
        }
    }
    
    func setRectPadding() -> UIEdgeInsets{
            return padding
        
    }
    
    override public func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: setRectPadding())
    }
    
    override public func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: setRectPadding())
    }
    
    override public func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: setRectPadding())
    }
    
    override init(frame: CGRect){
        super.init(frame: frame)
    }
    
    required public init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    //MARK: SHADOW PROPERTIES
    @IBInspectable
    var shadowRadius: CGFloat {
        get {
            return layer.shadowRadius
        }
        set {
            layer.shadowRadius = newValue
        }
    }
    
    @IBInspectable
    var shadowOpacity: Float {
        get {
            return layer.shadowOpacity
        }
        set {
            layer.shadowOpacity = newValue
        }
    }
    
    @IBInspectable
    var shadowOffset: CGSize {
        get {
            return layer.shadowOffset
        }
        set {
            layer.shadowOffset = newValue
        }
    }
    
    @IBInspectable
    var shadowColor: UIColor? {
        get {
            if let color = layer.shadowColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                
                layer.shadowColor = color.cgColor
            } else {
                layer.shadowColor = nil
            }
        }
    }
}
