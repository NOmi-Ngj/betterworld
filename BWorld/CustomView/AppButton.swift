//
//  AppButton.swift
//  BWorld
//
//  Created by Nouman Gul on 30/09/2020.
//

import UIKit

@IBDesignable
final class AppButton: UIButton {
    
    // Programmatically: use the enum
    var classFont:FontClassType = .regular
    var classFontSize:CGFloat = 17.0
    override func layoutSubviews() {
        super.layoutSubviews()
        setup()
    }
    func setup() {
       
    }
    
    // IB: use the adapter
    @IBInspectable var cornerRadius: CGFloat = 0.0 {
        didSet {
            self.layer.cornerRadius = cornerRadius
        }
    }

    override init(frame: CGRect){
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }
    
}
