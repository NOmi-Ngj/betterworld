//
//  AppLable.swift
//  BWorld
//
//  Created by Nouman Gul on 30/09/2020.
//

import Foundation

import UIKit
class AppLabel : UILabel{
    // Programmatically: use the enum
    var classFont:FontClassType = .regular
    var classFontSize:CGFloat = 17.0
    override func layoutSubviews() {
        super.layoutSubviews()
        setupView()
    }

    func setupView(){
        self.font = UIFont.setFontsWithClassType(classType: classFont, size: self.classFontSize)
    }
    
    @IBInspectable var fontsAdapter:Int {
        get {
            return self.classFont.rawValue
        }
        set( fontIndex) {
           self.classFont = FontClassType(rawValue: fontIndex) ?? .regular
        }
    }
    @IBInspectable var fontsSize:CGFloat {
        get {
            return self.classFontSize
        }
        set( fontSize) {
           self.classFontSize = fontSize
        }
    }
    
    override init(frame: CGRect){
        super.init(frame: frame)
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
}
