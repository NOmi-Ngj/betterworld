//
//  Ext+UIViewController.swift
//  BWorld
//
//  Created by Invision-102 on 2/9/22.
//

import UIKit
import Kingfisher

extension UIViewController {
    
    static func getVC(_ storyBoard: Stortyboad) -> Self {
        
        func instanceFromNib<T: UIViewController>(_ storyBoard: Stortyboad) -> T {
            guard let vc = controller(storyBoard: storyBoard, controller: T.identifier) as? T else {
                fatalError("'\(storyBoard.rawValue)' : '\(T.identifier)' is Not exist")
            }
            return vc
        }
        return instanceFromNib(storyBoard)
    }
    
    static func controller(storyBoard: Stortyboad, controller: String) -> UIViewController {
        let storyBoard = storyBoard.stortBoard
        let vc = storyBoard.instantiateViewController(withIdentifier: controller)
        return vc
    }
    
    func confirmationPopupAlert(title Title:String, with Message:String,  completionHandler: @escaping (Bool)->Void){
        
        let alert = UIAlertController(title: Title, message: Message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
                                        switch action.style{
                                        case .default:
                                            print("Yes")
                                            completionHandler(true)
                                            break
                                            
                                        case .cancel:
                                            print("cancel")
                                            
                                        case .destructive:
                                            print("destructive")
                                        @unknown default:
                                            break
                                        }}))
        
        alert.addAction(UIAlertAction(title: "No", style: .default, handler: { action in
                                        switch action.style{
                                        case .default:
                                            print("default")
                                            completionHandler(false)
                                        case .cancel:
                                            print("cancel")
                                            
                                        case .destructive:
                                            print("destructive")
                                            
                                            
                                        @unknown default:
                                            break
                                        }}))
        self.present(alert, animated: true, completion: {
        })
    }
    
    func popupAlert(title Title:String, with Message:String, completionHandler: @escaping (Bool)->Void){
        
        let alert = UIAlertController(title: Title, message: Message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                                        switch action.style{
                                        case .default:
                                            print("OK")
                                            completionHandler(true)
                                            break
                                        case .cancel:
                                            print("cancel")
                                        case .destructive:
                                            print("destructive")
                                        @unknown default:
                                            break
                                        }}))
        self.present(alert, animated: true, completion: {})
    }
    
    func showSuccessToast(message:String){
        self.showToastAtPosition(type: .success, position: .bottom, message: message)
    }
    func showErrorToast(message:String){
        self.showToastAtPosition(type: .error, position: .top, message: message)
    }
    func showToastAtPosition(type: ToastType, position: ToastPosition, message:String){
        var toastStyle = ToastStyle.init()
        
        switch type {
        case .success:
            toastStyle.backgroundColor = #colorLiteral(red: 0, green: 0.5874116421, blue: 0.4696099758, alpha: 1)
            if(message.count>0){
                self.view.makeToast(message, duration: 5.0, position: position,  style: toastStyle)
            }
        case .error:
            toastStyle.backgroundColor = #colorLiteral(red: 0.946643889, green: 0.2848697305, blue: 0.3140226603, alpha: 1)
            if(message.count>0){
                self.view.makeToast(message, duration: 5.0, position: position,  style: toastStyle)
            }
            else{
                self.view.makeToast("API Message: ", duration: 5.0, position: position,  style: toastStyle)
            }
        case .general:
            toastStyle.backgroundColor = #colorLiteral(red: 0.2047705054, green: 0.1998539269, blue: 0.3014870584, alpha: 1)
            if(message.count>0){
                self.view.makeToast(message, duration: 5.0, position: position,  style: toastStyle)
            }
        }
    }
}


enum Stortyboad: String {
    case structure = "Structure"
    case main = "Main"
    case auth = "Authentication"
    case mediaPlay = "MediaPlay"

    var stortBoard: UIStoryboard {
        return UIStoryboard(name: rawValue, bundle: Bundle.main)
    }
}

extension NSObject {
    class var identifier: String {
        return String(describing: self)
    }
}

//MARK:- ======== ViewController Identifiers ========
extension UIView
{
    static func getNib() -> Self {
        
        func instanceFromNib<T: UIView>() -> T {
            guard let vc = UINib(nibName: T.identifier, bundle: nil).instantiate(withOwner: nil, options: nil).last as? T else {
                fatalError("'\(T.identifier)' NIB is Not exist")
            }
            return vc
        }
        return instanceFromNib()
    }
}

extension UITableView {

    func registerTVC<cell:UITableViewCell> (_ : cell.Type) {
        self.register(UINib(nibName: cell.identifier, bundle: nil), forCellReuseIdentifier: cell.identifier)
    }
}

extension UICollectionView{
    
    func registerCVC<cell:UICollectionViewCell> (_ : cell.Type) {
        self.register(UINib(nibName: cell.identifier, bundle: nil), forCellWithReuseIdentifier: cell.identifier)
    }
}

public extension UIWindow {
    static var key: UIWindow? {
        if #available(iOS 13, *) {
            return UIApplication.shared.windows.first { $0.isKeyWindow }
        } else {
            return UIApplication.shared.keyWindow
        }
    }
}

extension UIApplication {

static func topViewController(controller: UIViewController? = UIWindow.key?.rootViewController) -> UIViewController {
    if let navigationController = controller as? UINavigationController {
        return topViewController(controller: navigationController.visibleViewController)
    }
    if let tabController = controller as? UITabBarController {
        if let selected = tabController.selectedViewController {
            return topViewController(controller: selected)
        }
    }
    if let presented = controller?.presentedViewController {
        return topViewController(controller: presented)
    }
    
    if controller == nil {
        return UIViewController()
    }
    return controller!
}
}


enum FontsType :String{
    case regular = "Poppins-Regular"
    case bold = "Poppins-Bold"
    case semiBold = "Poppins-SemiBold"
    case light = "Poppins-Light"
    case medium = "Poppins-Medium"
//    case hyregular = "HyundaiSansHeadOffice-Regular"
//    case hymedium = "HyundaiSansHeadOffice-Medium"
}
enum FontClassType :Int{
    case regular = 0
    case bold = 1
    case semiBold = 2
    case light = 3
    case medium = 4
    
    func returnFontType() -> FontsType{
        switch self {
        case .regular:
            return FontsType.regular
        case .bold:
            return FontsType.bold
        case .semiBold:
            return FontsType.semiBold
        case .light:
            return FontsType.light
        case .medium:
            return FontsType.medium
        }
    }
}

extension UIFont {
    static func setFontsWithType(type :FontsType, size:CGFloat) -> UIFont {
        
        return UIFont(name: type.rawValue, size: size) ?? UIFont(name: "AvenirNext-Regular", size: size)!
    }
    
    static func setFontsWithClassType(classType:FontClassType, size:CGFloat) -> UIFont {
        
        return self.setFontsWithType(type: classType.returnFontType(), size: size)
    }
}

extension UIImageView{
    
    func imageFromStringURL(from link: String){
        
        guard let url = URL(string: link) else {
            debugPrint("NOT A VALID URL \(link)")
            return
        }
        self.kf.setImage(with: url)
    }
    func imageFromURL(from link: URL?){
        if let url:URL = link {
//            self.downloadImage(from: url)
            self.kf.setImage(with: url)
            self.kf.indicatorType = .activity
        }
    }
    func downloadImage(from url: URL) {
        print("Download Started")
        getData(from: url) { data, response, error in
            guard let data = data, error == nil else { return }
            print(response?.suggestedFilename ?? url.lastPathComponent)
            print("Download Finished")
            // always update the UI from the main thread
            DispatchQueue.main.async() { [weak self] in
                self?.image = UIImage(data: data)
            }
        }
    }
    
    func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
    }
}

extension Int{
    
    var toString:String?{
        return String(self) ?? ""
    }
  
   
}
extension Double {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}
