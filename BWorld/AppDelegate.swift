//
//  AppDelegate.swift
//  BWorld
//
//  Created by Invision-102 on 2/9/22.
//

import UIKit
@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        if AppDefaults.currentUser != nil && AppDefaults.rememberMe == true{
            RouteManager.shared.showHome()
        }else{
            RouteManager.shared.showLogin()
        }
        
        LocationManager.sharedInstance.startTrackingUser()
        return true
    }

}

