//
//  SliderView.swift
//  BWorld
//
//  Created by Kassim Denim on 22/02/2022.
//

import Foundation
import UIKit
import Kingfisher

enum SliderViewContentMode{
    case fitToScale
    case fillToScale
}



class SliderView: BaseViews {
    
    
    
    var images:[URL] = []{
        didSet {
            self.collectionView.reloadData()
            self.pageControl.numberOfPages = self.images.count
            self.collectionView.isHidden = !(self.images.count > 0)
            self.pageControl.currentPage = 0
        }
    }
    var sliderContentMode:SliderViewContentMode = SliderViewContentMode.fillToScale
    @IBOutlet weak var collectionView:UICollectionView!{
        didSet{
            self.collectionView.delegate = self
            self.collectionView.dataSource = self
            self.collectionView.registerCVC(SliderViewCell.self)
            
        }
    }
    var timer:Timer?
    var currentItemIndex = 0
    @IBOutlet weak var pageControl:UIPageControl!
    override func awakeFromNib() {
        Bundle.main.loadNibNamed(SliderView.identifier, owner: self, options: nil)
        self.initWithNib()
        timer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(self.update), userInfo: nil, repeats: true)

    }
    @objc func update() {
        // Something cool
        if images.count > 0 {
            if currentItemIndex == images.count{
                currentItemIndex = 0
            }
            self.pageControl.currentPage = self.currentItemIndex
            let indexPath = IndexPath(item: self.currentItemIndex, section: 0)
            self.collectionView.scrollToItem(at: indexPath, at: [.centeredVertically, .centeredHorizontally], animated: true)
            currentItemIndex += 1
        }
        
        
    }
}

extension SliderView: UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: SliderViewCell.identifier, for: indexPath) as! SliderViewCell
        let imageUrl = self.images[indexPath.row]
//        cell.slideImage.
        cell.slideImage.imageFromURL(from: imageUrl)
        switch sliderContentMode {
        case .fillToScale:
            cell.slideImage.contentMode = .scaleAspectFill
        case .fitToScale:
            cell.slideImage.contentMode = .scaleAspectFit
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.images.count
    }
    
    
}

extension SliderView:UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width, height: (collectionView.frame.size.height))
    }
}
