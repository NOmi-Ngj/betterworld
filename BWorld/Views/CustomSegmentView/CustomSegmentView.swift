//
//  CustomSegmentView.swift
//  BWorld
//
//  Created by Saad Ahmed on 24/03/2022.
//

import Foundation
import UIKit

enum EnumSegmentCounter{
    case increment, decrement
}

class CustomSegmentView : BaseViews {
      
    @IBOutlet weak var btnIncrement: UIButton!
    @IBOutlet weak var btnDecrement: UIButton!
    var treeCount : Int = 0
    @IBOutlet weak var lblTreeCount:UILabel!
    override func awakeFromNib() {
        Bundle.main.loadNibNamed(CustomSegmentView.identifier, owner: self, options: nil)
        self.initWithNib()
        self.setUpViewButtons(with: .increment, count: treeCount)
    }
    
    @IBAction func decreamentCount(_ sender:UIButton){
        treeCount -= 1
        self.setUpViewButtons(with: .decrement, count: treeCount)
    }
    
    func setUpViewButtons(with type:EnumSegmentCounter, count:Int){
        if type == .increment{
            btnDecrement.isEnabled = true
            setTreeCount()
            if count == 5{
                btnIncrement.isEnabled = false
            }
            if count == 0 || count == 1{
                btnDecrement.isEnabled = false
            }
        }else if type == .decrement{
            btnIncrement.isEnabled = true
            btnDecrement.isEnabled = true
            setTreeCount()
            if count == 0 || count == 1{
                btnDecrement.isEnabled = false
            }
        }
    }
    
    @IBAction func incrementCount(_ sender:UIButton){
        treeCount += 1
        self.setUpViewButtons(with: .increment, count: treeCount)
    }
    
    func setTreeCount(){
        self.lblTreeCount.text = "\(treeCount)"
    }
    
}
