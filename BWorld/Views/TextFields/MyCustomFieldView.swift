//
//  MyCustomFieldView.swift
//  BWorld
//
//  Created by Kassim Denim on 01/03/2022.
//

import UIKit
protocol MyCustomTextfieldDidChange{
    func textfieldDidChange(textField:UITextField,type:ValidatorType)
}
class MyCustomFieldView: BaseViews {

    @IBOutlet weak var txtField:AppTextField!{
        didSet{
            self.txtField.delegate = self
        }
    }
    @IBOutlet weak var secureButton:UIButton!
    @IBOutlet weak var lblError:UILabel!
    @IBOutlet weak var lblPlaceholder:UILabel!
    @IBOutlet weak var lblDescription:UILabel!
    var fieldDelegates: MyCustomTextfieldDidChange?
    var type:ValidatorType = .none
    var textFieldPlaceHolder:String = ""
    var textFieldDescrition:String = ""
    var matchTextFieldValue:String = ""
    var isValidData = false
    
    override func awakeFromNib() {
        Bundle.main.loadNibNamed(MyCustomFieldView.identifier, owner: self, options: nil)
        self.initWithNib()
        self.txtField.setKeyboardType(model: type)
        self.lblPlaceholder.text = ""
        self.txtField.placeholder = self.textFieldPlaceHolder
        self.txtField.addTarget(self, action: #selector(textfieldDidChange(textField:)), for: .editingChanged)
        self.lblError.text = ""
        self.lblDescription.text = self.textFieldDescrition
        self.resetTextField()
        if type == .password || type == .confirmPassword{
            self.secureButton.isHidden = false
        }else{
            self.secureButton.isHidden = true
        }
    }
    
    @objc func textfieldDidChange(textField:UITextField){
        self.validate()
    }
    
    func getText() -> String{
        return self.txtField.text ?? " "
    }
    func validate(){
        do{
            let _ = try txtField.text?.validatedText(validationType: self.type, matchWithValue: matchTextFieldValue)
            self.isValidData = true
            self.txtField.borderColor = #colorLiteral(red: 0.2961477041, green: 0.6550741196, blue: 0.1550028026, alpha: 1)
            self.lblError.text = ""
            self.resetTextField()
            
        }catch(let error){
            debugPrint(error.getErrorValidMessage())
            self.lblError.text = error.getErrorValidMessage()
            self.txtField.borderColor = .red
            self.isValidData = false
            self.resetTextField()
        }
        self.fieldDelegates?.textfieldDidChange(textField: txtField, type: self.type)
    }
    private func resetTextField(){
        if txtField.text == ""{
            self.lblError.text = ""
            self.txtField.borderColor = #colorLiteral(red: 0.640761137, green: 0.6425934434, blue: 0.6667215228, alpha: 0.850682947)
        }
    }
    
    @IBInspectable var placeholder: String = "" {
        didSet {
            self.textFieldPlaceHolder = self.placeholder
        }
    }
    
    @IBInspectable var descriptionText: String = "" {
        didSet {
            self.textFieldDescrition = self.descriptionText
        }
    }
    
    private let borderThickness: (active: CGFloat, inactive: CGFloat) = (active: 2, inactive: 0.5)
    private let placeholderInsets = CGPoint(x: 20, y: 6)
    private let inactiveBorderLayer = CALayer()
    private let activeBorderLayer = CALayer()
    private var activePlaceholderPoint: CGPoint = CGPoint.zero
    
    @IBAction func didSelectSecureField(sender:UIButton){
        sender.isSelected = self.txtField.isSecureTextEntry
        self.txtField.isSecureTextEntry = !self.txtField.isSecureTextEntry
    }
}

extension MyCustomFieldView:UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
