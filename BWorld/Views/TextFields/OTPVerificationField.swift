//
//  OTPVerificationField.swift
//  BWorld
//
//  Created by Kassim Denim on 04/03/2022.
//

import UIKit

protocol OTPFieldDelegate: AnyObject {
    func textFieldDidDelete(textField : OTPField)
}

class OTPField: UITextField {

    weak var myDelegate: OTPFieldDelegate?

    override func deleteBackward() {
        super.deleteBackward()
        myDelegate?.textFieldDidDelete(textField: self)
    }

}

protocol OTPVerificationFieldDelegates{
    func didFilledAllField(isValidData:Bool)
    func verifyCode(input string:String)
}

class OTPVerificationField: BaseViews {
    
    @IBOutlet var textFields: [OTPField]!
    @IBOutlet var textFieldsView: [UIView]!
    var otpDelegates: OTPVerificationFieldDelegates?
    var filledColor = #colorLiteral(red: 0.2961477041, green: 0.6550741196, blue: 0.1550028026, alpha: 1)
    var unFilledColor = UIColor.black
    
    override func awakeFromNib() {
        Bundle.main.loadNibNamed(OTPVerificationField.identifier, owner: self, options: nil)
        self.initWithNib()
        
        self.textFieldsView.forEach({
            $0.backgroundColor = unFilledColor
        })
        
        textFields.forEach({
            $0.text = ""
            $0.textColor = filledColor
            $0.tintColor = .gray
            $0.addTarget(self, action: #selector(textDidChange(_:)), for: .editingChanged)
            $0.delegate = self
            $0.keyboardType = .numberPad
            $0.myDelegate = self
            $0.textContentType = .oneTimeCode
        })
    }
}

//MARK:- UITEXTFIELD DELEGATES

extension OTPVerificationField : OTPFieldDelegate , UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string == ""  {return true} // this handles the otp tap
        if string == " " {return false}
        if string.count > 1 {
            for (index,char) in string.enumerated() {
                if index < textFields.count  {
                    textFields[index].text = String(char)
                }
            }
            textField.endEditing(true)
            return false
        }
        if textField.text?.count == 0 , string == "" {
            if let prevField = textFields.first(where: { $0.tag == textField.tag - 1 }) {
                
                prevField.becomeFirstResponder()
                
                let bottomView = textFieldsView.first(where:{$0.tag == textField.tag})
                bottomView?.backgroundColor = unFilledColor
            }else{
                textField.endEditing(true)
            }
            return false
        }
        if string == "" {
            return true
        }
        if let text = textField.text , text.count == 1 {
            if let text = textField.text , text.count == 1 {
                if let nextField = textFields.first(where: { $0.tag == textField.tag + 1 }) {
                    nextField.becomeFirstResponder()
                    nextField.text = string
                    let bottomView = textFieldsView.first(where:{$0.tag == nextField.tag})
                    bottomView?.backgroundColor = filledColor
                }else{
                    textField.endEditing(true)
                }
                self.validateFields()
            }
            return false
        }
        return true
    }
    @objc func textDidChange(_ textField : UITextField){
        if let text = textField.text , text.count == 1 {
            if let nextField = textFields.first(where: { $0.tag == textField.tag + 1 }) {
                nextField.becomeFirstResponder()
            }else{
                textField.endEditing(true)
            }
            let bottomView = textFieldsView.first(where:{$0.tag == textField.tag})
            bottomView?.backgroundColor = filledColor
        }else{
            let bottomView = textFieldsView.first(where:{$0.tag == textField.tag})
            bottomView?.backgroundColor = unFilledColor
        }
        
        self.validateFields()
    }
    func textFieldDidDelete(textField : OTPField) {
        if textField.text?.count == 0 {
            if let prevField = textFields.first(where: { $0.tag == textField.tag - 1 }) {
                prevField.becomeFirstResponder()
                let bottomView = textFieldsView.first(where:{$0.tag == textField.tag})
                bottomView?.backgroundColor = unFilledColor
            }else{
                textField.endEditing(true)
            }
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        validateFields()
    }
    
    func validateFields(){
        if textFields.first(where: {$0.text?.count == 0}) != nil {
            otpDelegates?.didFilledAllField(isValidData: false)
        }else{
            otpDelegates?.didFilledAllField(isValidData: true)
            
            var text = ""
            textFields.forEach({text += $0.text!})
            otpDelegates?.verifyCode(input: text)
        }
    }
}
