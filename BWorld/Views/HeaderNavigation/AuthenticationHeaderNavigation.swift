//
//  HeaderBackNavigation.swift
//  CAPAROL
//
//  Created by Nouman Gul on 30/09/2020.
//

import UIKit


class AuthenticationHeaderNavigation: BaseViews {

    @IBOutlet weak var lblBigTitle:UILabel!
    @IBOutlet weak var lblSmallTitle:UILabel!
    override func awakeFromNib() {
        Bundle.main.loadNibNamed(AuthenticationHeaderNavigation.identifier, owner: self, options: nil)
        self.initWithNib()
    }
}
