//
//  HeaderBackNavigation.swift
//  CAPAROL
//
//  Created by Nouman Gul on 30/09/2020.
//

import UIKit

class HeaderBackNavigation: BaseViews {

    @IBOutlet weak var lbTitle:AppLabel!
    @IBOutlet weak var btnBack:AppButton!
    @IBOutlet weak var btnMenu:AppButton!
    @IBOutlet weak var btnLogout:AppButton!
    @IBOutlet weak var btnWallet:AppButton!
    
    @IBOutlet weak var showSafe:UIView!
    @IBOutlet weak var isshowSafeEnabled:UIView?
    override func awakeFromNib() {
        Bundle.main.loadNibNamed(HeaderBackNavigation.identifier, owner: self, options: nil)
        self.initWithNib()
        if isshowSafeEnabled == nil {
            showTopWhiteView(isHidden: false)
        }else{
            showTopWhiteView(isHidden: true)
        }
        
//        if(LocalizationHelper.isArabicSet()){
//            btnBack.transform = CGAffineTransform(scaleX: -1, y: 1)
//        }
    }
    
    func showTopWhiteView(isHidden:Bool){
        showSafe.isHidden = isHidden
    }
    func setTitle(title str:String){
        self.lbTitle.text = str
    }
    
    func showLogoutButton(){
        btnLogout.isHidden = false
//        if(LocalizationHelper.isArabicSet()){
//            btnLogout.transform = CGAffineTransform(scaleX: -1, y: 1)
//        }
    }
    func hideLogoutButton(){
        btnLogout.isHidden = true
    }
    
    func hideBackButton(){
        btnBack.isHidden = true
    }
    
    func showMenuHideBackButton(){
        btnBack.isHidden = true
        btnMenu.isHidden = false
    }
    
    func showBackHideMenuButton(){
        btnBack.isHidden = false
        btnMenu.isHidden = true
    }
    
    @IBAction func goBackTapped(_ sender:UIButton){
        delegates?.didTabBack()
    }
    
    @IBAction func goMenuTapped(_ sender:UIButton){
        delegates?.openDrawwerMenu()
    }
    
    @IBAction func goLogoutTapped(_ sender:UIButton){
        delegates?.didLogout()
    }
    
    @IBAction func goWalletTapped(_ sender:UIButton){
        delegates?.pushAddToWallet()
    }
}
