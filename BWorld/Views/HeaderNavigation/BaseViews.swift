//
//  BaseViews.swift
//  CAPAROL
//
//  Created by Nouman Gul on 30/09/2020.
//

import UIKit

protocol viewDelegates {
    func didTabBack()
    func openDrawwerMenu()
    func didLogout()
    func didTypeTextField()
    func didPressSearch()
    func pushAddToWallet()
    func pushEditProfile()
    func presentFilter()
}
extension viewDelegates{
    
    func presentFilter(){
        
    }
    func didTypeTextField(){
        
    }
    func didPressSearch(){
        
    }
    func pushAddToWallet(){
        
    }
    func pushEditProfile(){
        
    }
}

class BaseViews: UIView{
    @IBOutlet private var view: UIView!
    var delegates:viewDelegates?
    

    func initWithNib(){
        view.translatesAutoresizingMaskIntoConstraints = false
        addSubview(view)
        setupLayout()
    }
    private func setupLayout() {
        NSLayoutConstraint.activate(
            [
                view.topAnchor.constraint(equalTo: topAnchor),
                view.leadingAnchor.constraint(equalTo: leadingAnchor),
                view.bottomAnchor.constraint(equalTo: bottomAnchor),
                view.trailingAnchor.constraint(equalTo: trailingAnchor),
            ]
        )
    }
    
}
