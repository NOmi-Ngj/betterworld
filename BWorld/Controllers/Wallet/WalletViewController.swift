//
//  WalletViewController.swift
//  BWorld
//
//  Created by KT-Macmini on 04/04/2022.
//

import UIKit

class WalletViewController: TabbarControllersBaseView {

    var mainHeadingArray = ["Get Enviroment Engaged", "Plant a Tree"]
    var subHeadingArray = ["Better World - make this world a better place", "Earn rewards by growing trees and play your part in making this place a Better World"]
    @IBOutlet weak var collectionView: UICollectionView!{
        didSet{
            self.collectionView.delegate = self
            self.collectionView.dataSource = self
            self.collectionView.registerCVC(GiffCollectionViewCell.self)
            
        }
    }
    
    @IBOutlet weak var pageController: UIPageControl!
    override func viewDidLoad() {
        super.viewDidLoad()
        pageController.numberOfPages = 2
        // Do any additional setup after loading the view.
    }
    

   
//
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension WalletViewController:UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        2
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GiffCollectionViewCell", for: indexPath) as! GiffCollectionViewCell
        cell.lblMainHeading.text = mainHeadingArray[indexPath.row]
        cell.lblSubHeading.text = subHeadingArray[indexPath.row]
//        let model = dataModel[indexPath.row]
//        cell.data = model
        return cell
    }
    
//    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
//        self.pageController.currentPage = indexPath.row
//    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
            if scrollView == self.collectionView {
                var currentCellOffset = self.collectionView?.contentOffset
                currentCellOffset?.x += (self.collectionView?.frame.width)! / 2
                
                if let indexPath = self.collectionView?.indexPathForItem(at: currentCellOffset! ) {
                    self.collectionView.scrollsToTop = true
                    pageController.currentPage = indexPath.row
                }
            }
        }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.frame.size.width), height: (collectionView.frame.size.height))
    }
    
    
}
