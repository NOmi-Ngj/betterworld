//
//  UserViewModel.swift
//  BWorld
//
//  Created by Kassim Denim on 16/03/2022.
//

import Foundation


class UserViewModel{
    var handlerFailure: ((String)-> Void)?
    var handlerSuccess: (()-> Void)?
    
    var requestRegisterUser:RequestRegisterUser?
    var requestloginUser:RequestLoginUser?
    var shouldRememberUserInformation:Bool = false
 
    
    func registerUser(){
        guard let requestRegisterUser = requestRegisterUser else {
            return
        }
        NetworkManager.registerUser(data:requestRegisterUser , completion: { result in
            switch result {
            case .success(let response):
                debugPrint(response)
                AppDefaults.currentUser = response.data
                self.handlerSuccess?()
                
            case .failure(let message):
                debugPrint(message)
                self.handlerFailure?(message.capitalized)
            }
        })
    }
    
    func loginUser(){
        guard let requestloginUser = requestloginUser else {
            return
        }
        NetworkManager.loginUser(data:requestloginUser , completion: { result in
            switch result {
            case .success(let response):
                debugPrint(response)
                
                DispatchQueue.main.async {
                    AppDefaults.rememberMe = self.shouldRememberUserInformation
                    AppDefaults.currentUser = response.data
                }
                self.handlerSuccess?()
                
            case .failure(let message):
                debugPrint(message)
                self.handlerFailure?(message.capitalized)
            }
        })
    }
    
}
