//
//  LoginViewController.swift
//  BWorld
//
//  Created by Kassim Denim on 01/03/2022.
//

import UIKit

class LoginViewController: AuthBaseViewController {

    @IBOutlet weak var txtEmail:MyCustomFieldView!{
        didSet{
            self.txtEmail.type = .email
            self.txtEmail.fieldDelegates = self
        }
    }
    @IBOutlet weak var submitButton:UIButton!
    @IBOutlet weak var rememberMeButton:UIButton!
    
    @IBOutlet weak var txtPassword:MyCustomFieldView!{
        didSet{
            self.txtPassword.type = .password
            self.txtPassword.fieldDelegates = self
        }
    }
    
    @IBOutlet weak var txtPhoneNumber:MyCustomFieldView!{
        didSet{
            self.txtPhoneNumber.type = .confirmPassword
            self.txtPhoneNumber.fieldDelegates = self
        }
    }
    
    
    var viewModel = UserViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.headerSmallTitle = "Welcome"
        self.validateBinding()
        
        viewModel.handlerSuccess = {
            RouteManager.shared.showHomeAnimated()
        }
        
        viewModel.handlerFailure = { message in
            self.showErrorToast(message: message)
        }
        
        self.txtEmail.txtField.text = "Nouman@gmail.com"
        self.txtPassword.txtField.text = "123!@#qweQ"
        
        self.txtEmail.validate()
        self.txtPassword.validate()

        self.validateBinding()
    }
    
    func validateBinding(){
        let isValidData = self.validateData()
            
        submitButton.alpha = isValidData ? 1:0.4
        submitButton.isEnabled = isValidData ? true:false
    }
    
    func validateData()->Bool{
        let validEmail = txtEmail.isValidData
        let validPassword = txtPassword.isValidData
        let isValidData = validPassword == true && validEmail == true
        return isValidData
    }
    @IBAction func didTouchForgotPassword(sender:UIButton){
//        self.navigationBack
        let forgotPassword = ForgotPasswordViewController.getVC(.auth)
        self.navigationController?.pushViewController(forgotPassword, animated: true)
    }
    @IBAction func didTouchSignUp(sender:UIButton){
        
        let signUpVC = SIgnUpViewController.getVC(.auth)
        self.navigationController?.pushViewController(signUpVC, animated: true)
    }
    @IBAction func didTouchRemeberMe(sender:UIButton){
        sender.isSelected = !sender.isSelected
    }
    
    @IBAction func didTouchSubmit(sender:UIButton){
        if validateData(){
            viewModel.shouldRememberUserInformation = rememberMeButton.isSelected
            let request = RequestLoginUser(email: self.txtEmail.getText().lowercased(), password: self.txtPassword.getText())
            viewModel.requestloginUser = request
            viewModel.loginUser()
            
        }
        
    }
}

extension LoginViewController:MyCustomTextfieldDidChange{
    func textfieldDidChange(textField: UITextField, type: ValidatorType) {
        self.validateBinding()
    }
}
