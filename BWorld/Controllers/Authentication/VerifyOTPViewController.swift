//
//  VerifyOTPViewController.swift
//  BWorld
//
//  Created by Kassim Denim on 01/03/2022.
//

import UIKit

class VerifyOTPViewController: AuthBaseViewController {

    @IBOutlet weak var txtOTPVerification:OTPVerificationField!{
        didSet{
            self.txtOTPVerification.otpDelegates = self
        }
    }
    @IBOutlet weak var lblMinutesRemaining: AppLabel!
    @IBOutlet weak var submitButton:UIButton!
    var verificationCode = ""
    var counter = 0
    var timer = Timer()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.headerSmallTitle = "For Verification"
        self.didFilledAllField(isValidData: false)
        
        


    }
    // must be internal or public.
    @objc func update() {
        // Something cool
        print("Done!")
        counter += 1
        let threeMinutesInSecond = 180
        let count = threeMinutesInSecond-counter
        self.lblMinutesRemaining.text = self.convertIntegerToFormat(remainingTime: count)
        
        if count == 0{
            timer.invalidate()
            
        }
    }
    
    @objc func countBegin() {
        counter = 0
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.update), userInfo: nil, repeats: true)
    
    }
        func convertIntegerToFormat(remainingTime: Int) -> String {
                let seconds = remainingTime % 60
                let minutes = (remainingTime / 60) % 60
                return "\(minutes): \(seconds)"
        }
    
    @IBAction func didTouchSubmit(sender:UIButton){
        countBegin()
    }
}

extension VerifyOTPViewController:OTPVerificationFieldDelegates{
    func verifyCode(input string: String) {
        debugPrint(string)
        verificationCode = string
    }
    
    
    func didFilledAllField(isValidData: Bool) {
        verificationCode = ""
        submitButton.alpha = isValidData ? 1:0.4
        submitButton.isEnabled = isValidData ? true:false
    }
    
    
}
