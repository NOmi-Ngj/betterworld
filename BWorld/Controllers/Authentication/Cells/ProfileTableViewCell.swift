//
//  ProfileTableViewCell.swift
//  BWorld
//
//  Created by KT-Macmini on 05/04/2022.
//

import UIKit

class ProfileTableViewCell: UITableViewCell {

    @IBOutlet weak var btnMale: UIButton!
    @IBOutlet weak var btnFemale: UIButton!
    
    let radioController: RadioButtonController = RadioButtonController()
    override func awakeFromNib() {
        super.awakeFromNib()
        radioController.buttonsArray = [btnMale,btnFemale]
         radioController.defaultButton = btnMale
        // Initialization code
    }

    @IBAction func btnMaleAction(_ sender: Any) {
        print("Female")
        radioController.buttonArrayUpdated(buttonSelected: sender as! UIButton)
    }
    @IBAction func btnFeMaleAction(_ sender: Any) {
        print("Male")
        radioController.buttonArrayUpdated(buttonSelected: sender as! UIButton)
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
