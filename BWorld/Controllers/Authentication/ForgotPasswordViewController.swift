//
//  ForgotPasswordViewController.swift
//  BWorld
//
//  Created by Kassim Denim on 01/03/2022.
//

import UIKit

class ForgotPasswordViewController: AuthBaseViewController {
    
    @IBOutlet weak var txtEmail:MyCustomFieldView!{
        didSet{
            self.txtEmail.type = .email
            self.txtEmail.fieldDelegates = self
        }
    }
    @IBOutlet weak var submitButton:UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.headerSmallTitle = "Forgot Password?"
        self.validateBinding()
    }
    
    func validateBinding(){
        let isValidData = txtEmail.isValidData
        submitButton.alpha = isValidData ? 1:0.4
        submitButton.isEnabled = isValidData ? true:false
    }

    @IBAction func didTouchSignIn(sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func didTouchSubmit(sender:UIButton){
        
    }
}

extension ForgotPasswordViewController:MyCustomTextfieldDidChange{
    func textfieldDidChange(textField: UITextField, type: ValidatorType) {
        self.validateBinding()
    }
}
