//
//  ProfileViewController.swift
//  BWorld
//
//  Created by KT-Macmini on 05/04/2022.
//

import UIKit

class ProfileViewController: TabbarControllersBaseView {

    @IBOutlet weak var tableView: UITableView!{
            didSet{
                self.tableView.delegate = self
                self.tableView.dataSource = self
                self.tableView.registerTVC(ProfileTableViewCell.self)
            }
        }
    override func viewDidLoad() {
        super.viewDidLoad()
//        tableView.rowHeight = UITableView.automaticDimension
        
    }
    

}

extension ProfileViewController:UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return (UIScreen.main.bounds.height) - 200
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ProfileTableViewCell.identifier, for: indexPath) as! ProfileTableViewCell
        return cell
    }
    
}
