//
//  CreateNewPasswordViewController.swift
//  BWorld
//
//  Created by Kassim Denim on 01/03/2022.
//

import UIKit

class CreateNewPasswordViewController: AuthBaseViewController {

    @IBOutlet weak var txtPassword:MyCustomFieldView!{
        didSet{
            self.txtPassword.type = .password
            self.txtPassword.fieldDelegates = self
        }
    }
    @IBOutlet weak var txtConfirmPassword:MyCustomFieldView!{
        didSet{
            self.txtConfirmPassword.type = .confirmPassword
            self.txtConfirmPassword.fieldDelegates = self
        }
    }
    @IBOutlet weak var submitButton:UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.headerSmallTitle = "Setup Your"
        
        self.txtPassword.txtField.text = "123!@#qweQ"
        self.txtConfirmPassword.txtField.text = "123!@#qwe"
        
        self.txtPassword.validate()
        self.txtConfirmPassword.validate()
        self.validateBinding()

    }
    
    func validateBinding(){
        let validPassword = txtPassword.isValidData
        let validConfirmPassword = txtConfirmPassword.isValidData
        
        let isValidData = validPassword == true && validConfirmPassword == true
            
        submitButton.alpha = isValidData ? 1:0.4
        submitButton.isEnabled = isValidData ? true:false
    }
    
    @IBAction func didTouchSubmit(sender:UIButton){
        
    }
}
extension CreateNewPasswordViewController:MyCustomTextfieldDidChange{
    func textfieldDidChange(textField: UITextField, type: ValidatorType) {
        switch type {
        case .password:
            self.txtConfirmPassword.matchTextFieldValue = self.txtPassword.txtField.text ?? ""
            self.txtConfirmPassword.validate()
        case .confirmPassword:
            self.txtConfirmPassword.matchTextFieldValue = self.txtPassword.txtField.text ?? ""
        default:
            debugPrint("")
        }
        self.validateBinding()
    }
}
