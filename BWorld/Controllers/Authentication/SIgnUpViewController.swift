//
//  SIgnUpViewController.swift
//  BWorld
//
//  Created by Kassim Denim on 01/03/2022.
//

import UIKit

class SIgnUpViewController: AuthBaseViewController {
    
    @IBOutlet weak var submitButton:UIButton!
    @IBOutlet weak var txtFirstName:MyCustomFieldView!{
        didSet{
            self.txtFirstName.type = .none
            self.txtFirstName.fieldDelegates = self
        }
    }
    @IBOutlet weak var txtLastName:MyCustomFieldView!{
        didSet{
            self.txtLastName.type = .none
            self.txtLastName.fieldDelegates = self
        }
    }
    @IBOutlet weak var txtEmail:MyCustomFieldView!{
        didSet{
            self.txtEmail.type = .email
            self.txtEmail.fieldDelegates = self
        }
    }
    
    
    @IBOutlet weak var txtPassword:MyCustomFieldView!{
        didSet{
            self.txtPassword.type = .password
            self.txtPassword.fieldDelegates = self
        }
    }
    @IBOutlet weak var txtConfirmPassword:MyCustomFieldView!{
        didSet{
            self.txtConfirmPassword.type = .confirmPassword
            self.txtConfirmPassword.fieldDelegates = self
        }
    }

    var viewModel = UserViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupView()
    }
    
    func setupView(){
        self.headerSmallTitle = "Create An"
        
        
        viewModel.handlerSuccess = {
            
        }
        
        viewModel.handlerFailure = { message in
            self.showErrorToast(message: message)
        }
        
        // Do any additional setup after loading the view.
        self.txtFirstName.txtField.text = "Nouman"
        self.txtLastName.txtField.text = "Nouman"
        self.txtEmail.txtField.text = "Nouman@gmail.com"
        self.txtPassword.txtField.text = "123!@#qweQ"
        self.txtConfirmPassword.txtField.text = "123!@#qweQ"
        
        self.txtFirstName.validate()
        self.txtLastName.validate()
        self.txtEmail.validate()
        self.txtPassword.validate()
        self.txtConfirmPassword.validate()
        
        self.validateBinding()
    }
    
    func validateBinding(){

        let isValidData = validateData()
            
        submitButton.alpha = isValidData ? 1:0.4
        submitButton.isEnabled = isValidData ? true:false
    }
    
    func validateData()->Bool{
        let validEmail = txtEmail.isValidData
        let validPassword = txtPassword.isValidData
        let validName = txtFirstName.isValidData
        let validlastName = txtLastName.isValidData
        let validConfirmPassword = txtConfirmPassword.isValidData
        
        let isValidData = validPassword == true && validEmail == true && validName == true && validlastName == true && validConfirmPassword == true
        return isValidData
    }
    
    @IBAction func didTouchSignIn(sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func didTouchSubmit(sender:UIButton){
        if validateData(){
            let request = RequestRegisterUser.init(email: self.txtEmail.getText(), password: self.txtPassword.getText(), firstname: self.txtFirstName.getText(), lastname: self.txtLastName.getText(), phone: " ", gender: " ", country: " ", address: " ")
            viewModel.requestRegisterUser = request
            viewModel.registerUser()
            
        }
//        let opt = VerifyOTPViewController.getVC(.auth)
//        self.navigationController?.pushViewController(opt, animated: true)
    }
}

extension SIgnUpViewController:MyCustomTextfieldDidChange{
    func textfieldDidChange(textField: UITextField, type: ValidatorType) {
        switch type {
        case .password:
            self.txtConfirmPassword.matchTextFieldValue = self.txtPassword.txtField.text ?? ""
            self.txtConfirmPassword.validate()
        case .confirmPassword:
            self.txtConfirmPassword.matchTextFieldValue = self.txtPassword.txtField.text ?? ""
        default:
            debugPrint("")
        }
        self.validateBinding()
    }
}
