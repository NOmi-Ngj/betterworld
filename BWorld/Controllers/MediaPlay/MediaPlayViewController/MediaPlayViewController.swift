//
//  MediaPlayViewController.swift
//  BWorld
//
//  Created by Kassim Denim on 20/02/2022.
//

import UIKit
import AVFoundation
import AVKit

class MediaPlayViewController: BaseViewController {

    @IBOutlet weak var sliderView:SliderView!
    var projectsList: [ResponseProjectDetail] = []
    var selectedIndex = 0
    var player :AVPlayer?
    var previewLayer :AVPlayerLayer?
    var url:URL?{
        didSet{
            player = AVPlayer.init(url: url!)
            previewLayer = AVPlayerLayer.init(player: player)
            previewLayer?.backgroundColor = UIColor.clear.cgColor
            previewLayer?.videoGravity = .resizeAspect
            NotificationCenter.default.addObserver(self, selector: #selector(self.didfinishplaying(note:)),name:NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: self.player?.currentItem)
            self.player?.play()
            player?.currentItem?.addObserver(self, forKeyPath: "playbackBufferEmpty", options: [.new], context: nil)
            player?.currentItem?.addObserver(self, forKeyPath: "playbackLikelyToKeepUp", options: [.new], context: nil)
            player?.currentItem?.addObserver(self, forKeyPath: "playbackBufferFull", options: [.new], context: nil)
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Create Swipe Gesture Recognizers
        self.sliderView.addGestureRecognizer(createSwipeGestureRecognizer(for: .up))
        self.sliderView.addGestureRecognizer(createSwipeGestureRecognizer(for: .down))

        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            if let naviBar = self.navigationBack{
                naviBar.showBackHideMenuButton()
            }
            self.playVideo(at: self.selectedIndex)
        }
    }
    
    override func didTabBack(){
        super.didTabBack()
        self.player?.pause()
        self.previewLayer?.removeFromSuperlayer()
    }
    @objc private func didSwipe(_ sender: UISwipeGestureRecognizer) {
        switch sender.direction {
            case .up:
                debugPrint("Up")
                if selectedIndex != (self.projectsList.count - 1) {
                    selectedIndex += 1
                    UIView.transition(with: self.view, duration: 2, options: .transitionCurlUp, animations: nil, completion: nil)
                }else { return }
            case .down:
                debugPrint("Down")
                if selectedIndex != 0{
                    selectedIndex -= 1
                    UIView.transition(with: self.view, duration: 2, options: .transitionCurlDown, animations: nil, completion: nil)
                }else { return }
            default:
                break
        }
     self.playVideo(at: self.selectedIndex)
 }
    
    private func createSwipeGestureRecognizer(for direction: UISwipeGestureRecognizer.Direction) -> UISwipeGestureRecognizer {
        // Initialize Swipe Gesture Recognizer
        let swipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(didSwipe(_:)))
        // Configure Swipe Gesture Recognizer
        swipeGestureRecognizer.direction = direction
        return swipeGestureRecognizer
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {

        if object is AVPlayerItem {
            switch keyPath {
                case "playbackBufferEmpty":
                    UIApplication.startActivityIndicator()
                case "playbackLikelyToKeepUp":
                    UIApplication.stopActivityIndicator()
                case "playbackBufferFull":
                    UIApplication.stopActivityIndicator()
            default:
                debugPrint("1234")
            }
        }
    }
}

extension MediaPlayViewController{
    @objc func didfinishplaying(note : NSNotification){
        self.previewLayer?.removeFromSuperlayer()
    }
    
    func playVideo(at index:Int){
        self.previewLayer?.removeFromSuperlayer()
        let data = self.projectsList[index]
        self.sliderView.images = data.returnURlThumbnails()
        self.sliderView.sliderContentMode = .fitToScale
        if let urlString = data.videourl,let myUrl = URL(string: urlString){
            self.url = myUrl
        }
        self.sliderView.layer.addSublayer(previewLayer!)
        self.previewLayer?.backgroundColor = UIColor.black.cgColor
        self.previewLayer?.frame = self.sliderView.bounds
    }
}
