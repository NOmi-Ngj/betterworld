//
//  BaseViewController.swift
//  BWorld
//
//  Created by Invision-102 on 2/10/22.
//

import UIKit
import LGSideMenuController

protocol HelpingStartegy {
    var headerTitle : String {get set}
}

class BaseViewController: UIViewController,HelpingStartegy {
    var headerTitle: String = "" {
        didSet{
            self.navigationBack?.lbTitle.text = headerTitle
        }
    }
    

    @IBOutlet weak var navigationBack:HeaderBackNavigation?{
        didSet{
            self.navigationBack?.delegates = self
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        headerTitle = self.title ?? ""
    }
    
    @IBAction func goBack(_ sender:UIButton){
        showBackScreen()
    }
    
    func showBackScreen() {
        self.navigationController?.popViewController(animated: true)
    }
    func showRootController() {
        self.navigationController?.popToRootViewController(animated: true)
    }

}

extension BaseViewController:viewDelegates{
    @objc func didTabBack() {
        self.showBackScreen()
    }
    
    func openDrawwerMenu() {
        self.sideMenuController?.showLeftView()
    }
    
    func didLogout() {
        
    }
    
    func pushAddToWallet() {
    
    }
}
 
