//
//  AuthBaseViewController.swift
//  BWorld
//
//  Created by Kassim Denim on 01/03/2022.
//

import UIKit

protocol AuthHelpingStartegy {
    var headerTitle : String {get set}
    var headerSmallTitle : String {get set}
}

class AuthBaseViewController: UIViewController, AuthHelpingStartegy {
    var headerSmallTitle: String = "" {
        didSet{
            self.navigationBack?.lblSmallTitle.text = headerSmallTitle
        }
    }
    var headerTitle: String = "" {
        didSet{
            self.navigationBack?.lblBigTitle.text = headerTitle
        }
    }
    

    @IBOutlet weak var navigationBack:AuthenticationHeaderNavigation?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        headerTitle = self.title ?? ""
        // Do any additional setup after loading the view.
    }

}
