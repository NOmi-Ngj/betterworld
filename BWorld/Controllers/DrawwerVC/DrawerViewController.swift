//
//  DrawerViewController.swift
//  Kassim-Denim
//
//  Created by Kassim Denim on 24/09/2021.
//

import UIKit
import LGSideMenuController

class DrawerViewController: LGSideMenuController {

    override func viewDidLoad() {
        super.viewDidLoad()
        leftViewController = LeftDrawerViewController.getVC(.structure)
        leftViewWidth = self.view.bounds.width/1.2;
//        if let style = LGSideMenuPresentationStyle(rawValue: LGSideMenuPresentationStyle.slideBelow.rawValue) {
//        leftViewPresentationStyle = .slideAbove
////        }
//        isLeftViewSwipeGestureEnabled = false
        // Do any additional setup after loading the view.
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        if !isLeftViewStatusBarHidden {
            leftView?.frame = CGRect(x: 0.0, y: 00.0, width: self.view.bounds.width/0.5, height: self.view.bounds.height - 20.0)
        }
    }
//    override func leftViewWillLayoutSubviews(with size: CGSize) {
//        super.leftViewWillLayoutSubviews(with: size)
//
//       
//    }
    
    override var isLeftViewStatusBarHidden: Bool {
        get {
            return super.isLeftViewStatusBarHidden
        }
        
        set {
            super.isLeftViewStatusBarHidden = newValue
        }
    }
    deinit {
        print("Drawer deinitialized")
    }
    
}
