//
//  LeftDrawerViewController.swift
//  Kassim-Denim
//
//  Created by Kassim Denim on 24/09/2021.
//

import UIKit
import LGSideMenuController

enum LeftDrawwer{
    case profile
    case settings
    case logout
    
    func getViewController() -> UIViewController?{
        
        switch self {
        case .profile:
          return ProfileViewController.getVC(.auth)
        case .settings:
            return ViewController.getVC(.main)
        case .logout:
            return ViewController.getVC(.main)
        }
//        return ViewController.getVC(.main)
    }

    
    func getImage() -> UIImage{
        if let image = UIImage(systemName:getImageName()){
            return image
        }else{
            return #imageLiteral(resourceName: "drawer_icon")
        }
    }
    func getImageName() -> String{
        switch self {
        case .profile:
            return "person.crop.circle.badge.exclamationmark"
        case .settings:
            return "gearshape.fill"
        case .logout:
            return "ipad.and.arrow.forward"
        }
        
        return "info.circle.fill"
    }
    
    func getTitle() ->String{
        switch self {
        case .profile:
            return "Profile"
        case .settings:
            return "Settings"
        case .logout:
            return "Logout"
        }
    }
}

struct LeftMenuDataSource{
    var image:UIImage!
    var title:String!
    var shouldNavigate = true
    var vc:UIViewController!
    var viewModel:LeftDrawwer!
    static func initModel(model:LeftDrawwer) -> LeftMenuDataSource{
        return LeftMenuDataSource(image: model.getImage(), title: model.getTitle(), shouldNavigate: true, vc: model.getViewController(), viewModel: model)
    }
    static func staticData() -> [LeftMenuDataSource]{
         return [
            LeftMenuDataSource.initModel(model: .profile),
            LeftMenuDataSource.initModel(model: .settings),
            LeftMenuDataSource.initModel(model: .logout)
        ]
    }
}

class LeftDrawerViewController: UIViewController {

    @IBOutlet var tableView: UITableView!{
        didSet{
            self.tableView.registerTVC(DrawerTableViewCell.self)
            self.tableView.delegate = self
            self.tableView.dataSource = self
        }
    }
    
    var viewDataSource:[LeftMenuDataSource] = LeftMenuDataSource.staticData()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.reloadData()
        
    }
}
extension LeftDrawerViewController: UITableViewDelegate ,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewDataSource.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: DrawerTableViewCell.identifier, for: indexPath) as! DrawerTableViewCell
        let model = self.viewDataSource[indexPath.row]
        
        cell.lblTitle.text = model.title
        cell.imgIcon.image = model.image
        cell.imgIcon.tintColor = #colorLiteral(red: 0.05744218081, green: 0.8438857794, blue: 0.7728530765, alpha: 1)
        cell.contentView.backgroundColor = .clear
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let model = self.viewDataSource[indexPath.row]
        switch model.viewModel{
        case .logout:
            RouteManager.shared.showLogin()
        default:
            self.navigateToVC(vc: model.vc.self)
        }
    }
    

    
    func navigateToVC<T:UIViewController>(vc:T){
        if let sideMenu = UIApplication.topViewController() as? DrawerViewController {
            if let tabController = sideMenu.rootViewController as? UITabBarController, let navController = tabController.selectedViewController as? UINavigationController{
                sideMenu.hideLeftView(animated: true) {}
                if navController.fhkFindFirst(T.self) != nil{
                    navController.pushViewController(vc, animated: true)
                }else{
                    if let vc = navController.fhkFindFirst(T.self){
                        navController.popToViewController(vc, animated: true)
                    }
                }
            }
        }else{
            debugPrint("Top view is not drawwer ")
        }
    }
    
    func getTopViewNavigationVC() -> UINavigationController{
        if let sideMenu = UIApplication.topViewController() as? DrawerViewController {
            if let navController = sideMenu.rootViewController as? UINavigationController{
                return navController
            }
        }
        return UINavigationController()
    }
}

extension UINavigationController {
    func fhkFindFirst<T: UIViewController>(_: T.Type) -> T? {
        for viewController in viewControllers {
            if let viewController = viewController as? T {
                return viewController
            }
        }
        return nil
    }
}
