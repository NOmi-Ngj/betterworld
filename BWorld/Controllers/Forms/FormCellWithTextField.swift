//
//  FormCellWithTextField.swift
//  BWorld
//
//  Created by Invision-102 on 2/10/22.
//

import UIKit

protocol CellWithOptionsDelegates {
    func cell(indexOfCell:Int , type:TypeOfCell , value:String)
    func cell(indexOfCell:Int , type:TypeOfCell , value:[String])
}

class FormCellWithTextField: UITableViewCell {

    @IBOutlet weak var lb_title : UILabel!
    @IBOutlet weak var lb_validation : UILabel!
    @IBOutlet weak var txtField : UITextField!
    var headerTitle = ""
    var model : ModelForCells?
    var delegate:CellWithOptionsDelegates!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.txtField.addTarget(self, action: #selector(textfieldDidChange(textField:)), for: .editingChanged)
        self.addDoneButtonOnKeyboard()
    }
    
    func addDoneButtonOnKeyboard(){
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        doneToolbar.barStyle = .default

        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneButtonAction))

        let items = [flexSpace, done]
        doneToolbar.items = items
        doneToolbar.sizeToFit()

        txtField.inputAccessoryView = doneToolbar
    }

    @objc func doneButtonAction(){
        txtField.resignFirstResponder()
    }
    
    @objc func textfieldDidChange(textField:UITextField){
        
        switch (model?.validationTypes)! {
        case .none:
            
            break
        case .number:
            
            break
        case .email:
            
            break
        case .website:
            txtField.setUpWebsite()
            break
        default:
            break
        }
        delegate.cell(indexOfCell: self.tag, type: TypeOfCell.textFieldCell, value: textField.text ?? " ")
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    func setUpView(view:ModelForCells){
        self.model = view
        let title = view.titleText ?? ""
        if view.shouldValidate{
            lb_validation.text =  view.validationText
        }else{
            lb_validation.text = ""
        }
        self.lb_title.text = title
        self.lb_title.textColor = .black
        self.txtField.textColor = UIColor.green
        self.txtField.placeholder = view.placeHolderText
        self.txtField.text = view.inputValue
        self.txtField.setKeyboardType(model: view.validationTypes)
        self.txtField.keyboardType = view.inputType
    }
    
}
