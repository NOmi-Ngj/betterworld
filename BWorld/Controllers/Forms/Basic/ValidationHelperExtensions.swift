//
//  ValidationHelperExtensions.swift
//  BWorld
//
//  Created by Invision-102 on 2/10/22.
//

import UIKit

extension UITextField{
    
    func setUpWebsite(){
        
        if self.text?.count == 1{
            self.text = "https://www.\(self.text ?? "")"
        }else{
            if self.text == "https://www."{
                self.text = ""
            }
        }
    }
    func setKeyboardType(model: ValidatorType){
        switch model {
        case .none:
            self.keyboardType = .default
            break
        case .number:
            self.keyboardType = .numberPad
            break
        case .email:
            self.keyboardType = .emailAddress
            break
        case .website:
            self.keyboardType = .default
            break
        case .password, .confirmPassword:
            self.isSecureTextEntry = true
            self.keyboardType = .default
        default:
            break
        }
    }
    
}
