//
//  FormsModel.swift
//  BWorld
//
//  Created by Invision-102 on 2/10/22.
//

import UIKit


struct ModelForCells {
    var type : TypeOfCell
    var shouldValidate : Bool
    var validationTypes : ValidatorType
    var validationText = " *"
    var titleText : String?
    var placeHolderText : String?
    var inputValue = ""
    var inputValueArray : [String]?
    var inputType : UIKeyboardType!
    var pickerData : [String]?
    var images = [UIImage]()
    var serviceKey : RequestHelpDesk.CodingKeys
    init(type:TypeOfCell , shouldValidate : Bool , title: String ,placeHolderText:String,serviceKey: RequestHelpDesk.CodingKeys , vadliationType:ValidatorType,inputType : UIKeyboardType = UIKeyboardType.default) {
        self.type = type
        self.shouldValidate = shouldValidate
        self.titleText = title
        self.placeHolderText = placeHolderText
        self.serviceKey = serviceKey
        self.validationTypes = vadliationType
        self.inputType = inputType
    }
    
    mutating func setPickerData(data : [String])  {
        self.pickerData = data
    }
    
    mutating func setMultiSelectionData(data : [String])  {
        self.inputValueArray = data
    }
    
    
    mutating func setImages(images : [UIImage])  {
        self.images = images
    }
    
}
struct RequestHelpDesk : Codable {
    var name:String?
    enum CodingKeys: String, CodingKey {
        case name = ""
    }
}

enum TypeOfCell {
    case textFieldCell, textViewCell, downPicker, attachement, calender, segentedControl, multiSelectionDropDown
}
