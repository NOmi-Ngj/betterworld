//
//  FormValidatorType.swift
//  BWorld
//
//  Created by Invision-102 on 2/10/22.
//

import UIKit
protocol ValidatorConvertible {
    func validated(_ value: String?) throws -> String
}

enum ValidatorType {
    case none,
    email,
    website,
    number,
    password,
    confirmPassword
}

enum VaildatorFactory {
    static func validatorFor(type: ValidatorType, matchWithValue:String = "") -> ValidatorConvertible {
        switch type {
        case .email:
            return EmailValidator()
        case .website:
            return WebsiteValidator()
        case .number:
            return NumberValidator()
        case .password:
            return PasswordValidator()
        case .confirmPassword:
            return ConfirmPasswordValidator.init(password: matchWithValue)
        case .none: return NoValidation()
        
            
        }
    }
}
struct ConfirmPasswordValidator:ValidatorConvertible{
    private var password:String = ""

    init(password:String) {
        self.password = password
    }
    func validated(_ value: String?) throws -> String {
        var matchPassword = ""
        var passwordTillLength = ""
        if password.count >= value?.count ?? 0{
            
            if let countValue:Int = value?.count, let objValue:String = value{
                matchPassword = objValue.substring(toIndex: countValue)
                passwordTillLength = password.substring(toIndex: countValue)
            }
            
            if matchPassword == passwordTillLength{
                if password.length == value?.length{
                    return value!
                }
                throw ValidationError("Confirm password matching with password field")
            }else{
                throw ValidationError("Confirm Password not match with password field")
            }
            debugPrint(matchPassword)
        }else{
            throw ValidationError("Confirm password length does not match")
        }
        let minCount = 8
        let maxCount = 32
        if value?.count ?? 0 <= minCount{
            throw ValidationError("Password should be more then \(minCount) words")
        }else if value?.count ?? 0 >= maxCount{
            throw ValidationError("Password should be maximum \(maxCount) words")
        }
        do {
            let regexPattern = "^(?=.*[A-Z])(?=.*[!@#$&*])(?=.*[0-9])(?=.*[a-z]).{\(minCount),\(maxCount)}$"
            let phoneTest = NSPredicate(format: "SELF MATCHES %@", regexPattern)
            if phoneTest.evaluate(with: value) {
                return value!
            }
            if value?.count ?? 0 < minCount{
                
            }else if value?.count ?? 0 > maxCount{
                
            }

            throw ValidationError("Weak password")
        }
    }
}
struct PasswordValidator:ValidatorConvertible{
    func validated(_ value: String?) throws -> String {
        let minCount = 8
        let maxCount = 32
        if value?.count ?? 0 <= minCount{
            throw ValidationError("Password should be more then \(minCount) words")
        }else if value?.count ?? 0 >= maxCount{
            throw ValidationError("Password should be maximum \(maxCount) words")
        }
        do {
            let regexPattern = "^(?=.*[A-Z])(?=.*[!@#$&*])(?=.*[0-9])(?=.*[a-z]).{\(minCount),\(maxCount)}$"
            let phoneTest = NSPredicate(format: "SELF MATCHES %@", regexPattern)
            if phoneTest.evaluate(with: value) {
                return value!
            }
            if value?.count ?? 0 < minCount{
                
            }else if value?.count ?? 0 > maxCount{
                
            }

            throw ValidationError("Weak password")
        }
    }
}

struct NumberValidator: ValidatorConvertible {
    func validated(_ value: String?) throws -> String {
        
        let minCount = 6
        let maxCount = 14
        if let word = value?.first, word != "+"{
            throw ValidationError("Number should start with +")
        }else if value?.count ?? 0 < minCount{
            throw ValidationError("Number should be more then \(minCount) words")
        }else if value?.count ?? 0 > maxCount{
            throw ValidationError("Number should be maximum \(maxCount) words")
        }
        do {
            let regexPattern = "^((\\+)|(00))[0-9]{\(minCount),\(maxCount)}$"
            let phoneTest = NSPredicate(format: "SELF MATCHES %@", regexPattern)
            if phoneTest.evaluate(with: value) {
                return value!
            }
            throw ValidationError("Invalid Number")
        }
    }
}
struct WebsiteValidator: ValidatorConvertible {
    func validated(_ value: String?) throws -> String {
    

        
        do {
            let regexPattern = "https?://(www|WWW)+\\.+\\S+\\.+\\S+"  //
            if try NSRegularExpression(pattern: regexPattern, options: .caseInsensitive).firstMatch(in: value!, options: [], range: NSRange(location: 0, length: value!.count)) == nil {
                throw ValidationError("Invalid Website Address")
            }
        } catch {
            throw ValidationError("Invalid Website Address")
        }
        return value!
    }
}


struct NoValidation: ValidatorConvertible {
    
    func validated(_ value: String?) throws -> String {
        let minCount = 1
        if value?.count ?? 0 < minCount{
                    throw ValidationError("Number should be more then \(minCount) words")
                }
        return value ?? ""
    }
}


struct IPAdressValidation: ValidatorConvertible {
    
    func validated(_ value: String?) throws -> String {
        do {
            let regexPattern = "^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$"
            if try NSRegularExpression(pattern: regexPattern, options: .caseInsensitive).firstMatch(in: value!, options: [], range: NSRange(location: 0, length: value!.count)) == nil {
                throw ValidationError("Invalid IP Address")
            }
        } catch {
            throw ValidationError("Invalid IP Address")
        }
        return value!
    }
}

struct RequiredFieldValidator: ValidatorConvertible {
    private let fieldName: String
    
    init(_ field: String) {
        fieldName = field
    }
    
    func validated(_ value: String?) throws -> String {
        guard !value!.isEmpty else {
            throw ValidationError("Required field " + fieldName)
        }
        return value!
    }
}

struct EmailValidator: ValidatorConvertible {
    func validated(_ value: String?) throws -> String {
        do {
            if try NSRegularExpression(pattern: "^[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}$", options: .caseInsensitive).firstMatch(in: value!, options: [], range: NSRange(location: 0, length: value!.count)) == nil {
                throw ValidationError("Invalid e-mail Address")
            }
        } catch {
            throw ValidationError("Invalid e-mail Address")
        }
        return value!
    }
}

struct LandlineExtension: ValidatorConvertible {
    
    private let fieldName: String
    
    init(_ field: String) {
        fieldName = field
    }
    
    func validated(_ value: String?) throws -> String {
        do {
            if try NSRegularExpression(pattern: "00971(2|4).\\d{7}", options: .caseInsensitive).firstMatch(in: value!, options: [], range: NSRange(location: 0, length: value!.count)) == nil {
                throw ValidationError("Invalid Extension")
            }
        } catch {
            throw ValidationError("Invalid Extension")
        }
        return value!
    }
}



class ValidationError: Error {
    var message: String
    
    init(_ message: String) {
        self.message = message
    }
}

extension String {
    func validatedText(validationType: ValidatorType, matchWithValue:String = "") throws -> String {
        let validator = VaildatorFactory.validatorFor(type: validationType, matchWithValue: matchWithValue)
        return try validator.validated(self)
    }
}

extension String {

    var length: Int {
        return count
    }

    subscript (i: Int) -> String {
        return self[i ..< i + 1]
    }

    func substring(fromIndex: Int) -> String {
        return self[min(fromIndex, length) ..< length]
    }

    func substring(toIndex: Int) -> String {
        return self[0 ..< max(0, toIndex)]
    }

    subscript (r: Range<Int>) -> String {
        let range = Range(uncheckedBounds: (lower: max(0, min(length, r.lowerBound)),
                                            upper: min(length, max(0, r.upperBound))))
        let start = index(startIndex, offsetBy: range.lowerBound)
        let end = index(start, offsetBy: range.upperBound - range.lowerBound)
        return String(self[start ..< end])
    }
}

extension Error{
    
    func getErrorValidMessage() -> String{
        if self is ValidationError{
            return (self as! ValidationError).message
        }else{
            return self.localizedDescription
        }
    }
    func getErrorValidMessageWithOutLocale() -> String{
        if self is ValidationError{
            return (self as! ValidationError).message
        }else{
            return self.localizedDescription
        }
    }
}

