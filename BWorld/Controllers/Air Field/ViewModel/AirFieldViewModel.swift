//
//  AirFieldViewModel.swift
//  BWorld
//
//  Created by Saad Ahmed on 30/03/2022.
//


import Foundation

class AirFieldViewModel{
    var handlerFailure: ((String)-> Void)?
    var handlerSuccess: (()-> Void)?
    var myAirFieldsRewards:ResponseAirFieldsRewards?
    
    func getGetAirFieldsRewards(){

        NetworkManager.getMyAirFieldsRewards { result in
            switch result {
            case .success(let response):
                debugPrint(response)
                self.myAirFieldsRewards = response
                self.handlerSuccess?()
                
            case .failure(let message):
                debugPrint(message)
            }
        }
    }
}
