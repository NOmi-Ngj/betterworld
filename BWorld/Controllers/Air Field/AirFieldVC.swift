//
//  AirFieldVC.swift
//  BWorld
//
//  Created by Kassim Denim on 11/02/2022.
//

import UIKit

class AirFieldVC: TabbarControllersBaseView {

 
    
    @IBOutlet weak var vwAirFieldHeaderView: AirFieldHeaderView!
    var total: Double = 0.0
    @IBOutlet weak var tableView:UITableView!{
        didSet{
            self.tableView.delegate = self
            self.tableView.dataSource = self
            self.tableView.registerTVC(AirFieldDetailCell.self)
        }
    }
    var viewModel = AirFieldViewModel()
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.getGetAirFieldsRewards()
        viewModel.handlerSuccess = {
            self.tableView.reloadData()
        }
    }
}

extension AirFieldVC:UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.myAirFieldsRewards?.data?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: AirFieldDetailCell.identifier, for: indexPath) as! AirFieldDetailCell
       
        let data = self.viewModel.myAirFieldsRewards?.data?[indexPath.row]
        total = total + (data?.air_rewarded ?? 0.0)
        vwAirFieldHeaderView.lblTotalCoin.text = String(total.rounded(toPlaces: 3))
        print(total.rounded(toPlaces: 3))
        cell.data = data
        return cell
    }
    
}



