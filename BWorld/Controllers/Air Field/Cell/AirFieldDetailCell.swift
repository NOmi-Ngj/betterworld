//
//  AirFieldDetailCell.swift
//  BWorld
//
//  Created by Kassim Denim on 11/02/2022.
//

import UIKit

class AirFieldDetailCell: UITableViewCell {

    @IBOutlet weak var lblAir_rewarded: UILabel!
    @IBOutlet weak var lblProjectName: UILabel!
    @IBOutlet weak var lblProjectDescription: UILabel!
    @IBOutlet weak var imgAirIcon: UIImageView!
    var data:ResponseAirFieldRewardsDetails?{
            didSet {
                self.lblAir_rewarded.text = String(data?.air_rewarded?.rounded(toPlaces: 2) ?? 0.0)
                        self.lblProjectName.text = data?.project
                        self.lblProjectDescription.text = data?.description
                        let imageURL = URL(string: data?.air_icon ?? "")
                        self.imgAirIcon.imageFromURL(from: imageURL)
            }
        }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
   
    
}
