//
//  AirFieldHeaderView.swift
//  BWorld
//
//  Created by Kassim Denim on 11/02/2022.
//

import UIKit

class AirFieldHeaderView: BaseViews {

    @IBOutlet weak var lblTotalCoin: UILabel!
    override func awakeFromNib() {
        Bundle.main.loadNibNamed(AirFieldHeaderView.identifier, owner: self, options: nil)
        self.initWithNib()
    }

}
