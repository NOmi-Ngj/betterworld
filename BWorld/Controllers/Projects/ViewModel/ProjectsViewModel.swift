//
//  ProjectsViewModel.swift
//  BWorld
//
//  Created by Kassim Denim on 19/02/2022.
//

import Foundation

class ProjectsViewModel{
    var handlerFailure: ((String)-> Void)?
    var handlerSuccess: (()-> Void)?
    var myProjects:ResponseResponseGetProjects?
    var publicProjects:ResponseResponseGetProjects?
    var requesMyProjects:RequestFindPublicProject?
    var requestPublicProjects:RequestFindPublicProject?
    var requestCreateNewProject:RequestCreateNewProject?
    var requestUploadProjectVideo:RequestUploadProjectVideo?
    
    
    
    func uploadProjectVideo(){
        guard let requestUploadProjectVideo = requestUploadProjectVideo else {
            return
        }
        if requestUploadProjectVideo.videoData == Data() {
            return
        }
        NetworkManager.uploadProjectVideo(data:requestUploadProjectVideo , completion: { result in
            switch result {
            case .success(let response):
                debugPrint(response)
                if response.status_code == 201{
                    self.handlerSuccess?()
                }
                
            case .failure(let message):
                debugPrint(message)
            }
        })
    }
    
    
    func getMyProjects(){
        guard let requesMyProjects = requesMyProjects else {
            return
        }

        NetworkManager.getMyProjects(data:requesMyProjects , completion: { result in
            switch result {
            case .success(let response):
                debugPrint(response)
                self.myProjects = response
                self.handlerSuccess?()
                
            case .failure(let message):
                debugPrint(message)
            }
        })
    }
    
    func getDummyRequest(){
        if let userEmail = AppDefaults.currentUser?.email{
            self.requestPublicProjects = RequestFindPublicProject(page: 0, size: 10, pollstatus: 0, email: userEmail)
        }
        
    }
    func getMyDummyRequest(){
        if let userEmail = AppDefaults.currentUser?.email{
            self.requesMyProjects = RequestFindPublicProject(page: 0, size: 10, pollstatus: 0, email: userEmail)
        }
    }
    
    func getPulblicProjects(){
        guard let requestPublicProjects = requestPublicProjects else {
            return
        }

        NetworkManager.getPublicProjects(data:requestPublicProjects , completion: { result in
            switch result {
            case .success(let response):
                debugPrint(response)
                self.publicProjects = response
                self.handlerSuccess?()
                
            case .failure(let message):
                debugPrint(message)
                self.handlerFailure?(message)
            }
        })
    }
    
    func createNewProject(){
        guard let requestCreateNewProject = requestCreateNewProject else {
            return
        }

        NetworkManager.createNewProject(data: requestCreateNewProject , completion: { result in
            switch result {
            case .success(let response):
                debugPrint(response)
                self.handlerSuccess?()
                
            case .failure(let message):
                debugPrint(message)
                self.handlerFailure?(message)
            }
        })
    }
    
}
