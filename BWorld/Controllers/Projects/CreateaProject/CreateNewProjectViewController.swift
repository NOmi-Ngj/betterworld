//
//  CreateNewProjectViewController.swift
//  BWorld
//
//  Created by Saad Ahmed on 24/03/2022.
//

import UIKit

class CreateNewProjectViewController: BaseViewController {
    
    @IBOutlet weak var txtName:MyCustomFieldView!{
        didSet{
            self.txtName.type = .none
            self.txtName.fieldDelegates = self
        }
    }
    
    @IBOutlet weak var txtDescription:MyCustomFieldView!{
        didSet{
            self.txtDescription.type = .none
            self.txtDescription.fieldDelegates = self
        }
    }
    
    @IBOutlet weak var vwSegmentView:CustomSegmentView!
    @IBOutlet weak var submitButton:UIButton!
    
    var viewModel = ProjectsViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationBack?.showBackHideMenuButton()
        self.validateBinding()
    }
    
    func validateBinding(){
        let isValidData = self.validateData()
        submitButton.alpha = isValidData ? 1:0.4
        submitButton.isEnabled = isValidData ? true:false
    }
    
    func validateData()->Bool{
        let validEmail = txtName.isValidData
        let validPassword = txtDescription.isValidData
        let isValidData = validPassword == true && validEmail == true
        return isValidData
    }

    @IBAction func didTouchSubmit(sender:UIButton){
        if validateData(){
            viewModel.requestCreateNewProject = RequestCreateNewProject(email: AppDefaults.currentUser?.email ?? "", projecttitle: self.txtName.getText(), projectdescription: self.txtDescription.getText(), totaltrees: vwSegmentView.treeCount.toString ?? "0", lat: String(LocationManager.sharedInstance.currentLocation!.currentLat ?? 0), lon: String(LocationManager.sharedInstance.currentLocation!.currentLng ?? 0), location: "Mandra Gujarkhan")
            viewModel.createNewProject()
        }
    }
}

extension CreateNewProjectViewController:MyCustomTextfieldDidChange{
    func textfieldDidChange(textField: UITextField, type: ValidatorType) {
        self.validateBinding()
    }
}
