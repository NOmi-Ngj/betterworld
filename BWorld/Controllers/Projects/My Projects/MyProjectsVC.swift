//
//  MyProjectsVC.swift
//  BWorld
//
//  Created by Kassim Denim on 10/02/2022.
//

import UIKit
import Floaty
import AVKit
import AVFoundation
import MobileCoreServices

class MyProjectsVC: TabbarControllersBaseView {
    
    @IBOutlet weak var collectionView:UICollectionView!{
        didSet{
            self.collectionView.delegate = self
            self.collectionView.dataSource = self
            self.collectionView.registerCVC(ProjectHeaderCell.self)
            
        }
    }
    @IBOutlet weak var tableView:UITableView!{
        didSet{
            self.tableView.registerTVC(ProjectDetailsCell.self)
            self.tableView.delegate = self
            self.tableView.dataSource = self
        }
    }
    
    @IBOutlet weak var vwFloaty: Floaty!{
        didSet{
            self.vwFloaty.addItem("", icon: UIImage(named: "plant_tree")!, colours: UIColor.init(named: "green_color")!, handler: { [weak self] item in
                self?.navigationController?.pushViewController(CreateNewProjectViewController.getVC(.mediaPlay), animated: true)
                self?.vwFloaty.close()
            })
            self.vwFloaty.addItem("", icon: UIImage(named: "good_heart")!, colours: UIColor.init(named: "green_color")!, handler: { [weak self] item in
                self?.vwFloaty.close()
            })
        }
    }
    
    var dataModel = ModelProjectHeader.ProjectHeaderStatic()
    var viewModel = ProjectsViewModel()
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.getMyDummyRequest()
        viewModel.getMyProjects()
        viewModel.handlerSuccess = {
            self.tableView.reloadData()
            self.collectionView.reloadData()
        }
    }
}

extension MyProjectsVC:UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataModel.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ProjectHeaderCell.identifier, for: indexPath) as! ProjectHeaderCell
        let model = dataModel[indexPath.row]
        cell.data = model
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        for (index,_) in dataModel.enumerated(){
            if index == indexPath.row{
                dataModel[index].isSelected = true
            }else{
                dataModel[index].isSelected = false
            }
        }
        self.collectionView.reloadData()
        self.tableView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.frame.size.width)/2, height: (collectionView.frame.size.height))
    }
}

extension MyProjectsVC:UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let selectedType = self.dataModel.filter({$0.isSelected == true}).first{
            if selectedType.name == .myProject {
                return viewModel.myProjects?.docs?.count ?? 0
            }else{
                return 0
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ProjectDetailsCell.identifier, for: indexPath) as! ProjectDetailsCell
        let data = self.viewModel.myProjects?.docs?[indexPath.row]
        cell.sliderView.images = data?.returnURlThumbnails() ?? []
        cell.data = data
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        let data = self.viewModel.myProjects?.docs?[indexPath.row]
        if data?.status == 0{
            self.confirmationPopupAlert(title: "Upload Video", with: "Are You sure tyou want to upload") { Value in
                if Value{
                    VideoRecorderManager.shared.viewController = self
                    VideoRecorderManager.shared.openCamera()
                    debugPrint("Upload video")
                }
            }
        }
       

        VideoRecorderManager.shared.didSuccessfullyRecordedVideo = { value in
            if value{
                if let videoData = VideoRecorderManager.shared.videoData{
//                    let request = RequestUploadProjectVideo(email: AppDefaults.currentUser?.email ?? "", projecid: data?.id ?? "", videoformat: "mp4", videoData: videoData )//RequestLoginUser(
                    let request = RequestUploadProjectVideo(email: AppDefaults.currentUser?.email ?? "", videoformat: data?.id ?? "", videoData: videoData, lat: LocationManager.sharedInstance.currentLocation?.currentLat ?? 0.0, lon: LocationManager.sharedInstance.currentLocation?.currentLng ?? 0.0, projectid: data?.id ?? "")
                    self.viewModel.requestUploadProjectVideo = request
                    self.viewModel.uploadProjectVideo()
                }
            }
        }
        
    }
}

extension MyProjectsVC:UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
}
