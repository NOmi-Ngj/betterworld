//
//  ProjectHeaderCell.swift
//  BWorld
//
//  Created by Kassim Denim on 11/02/2022.
//

import UIKit

enum ProjectType:String{
    case myProject = "MY PROJECT"
    case Misc = "MISC."
}

struct ModelProjectHeader{
    var name:ProjectType!
    var isSelected:Bool!
    
    static func ProjectHeaderStatic()->[ModelProjectHeader]{
        return [ModelProjectHeader(name: ProjectType.myProject, isSelected: true),
                ModelProjectHeader(name: ProjectType.Misc, isSelected: false)]
    }
}


class ProjectHeaderCell: UICollectionViewCell {

    @IBOutlet weak var lblName:UILabel!
    @IBOutlet weak var vwTagline:UIView!
    
    var data:ModelProjectHeader!{
        didSet{
            self.lblName.text = data.name.rawValue
            self.vwTagline.isHidden = !data.isSelected
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
