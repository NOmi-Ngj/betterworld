//
//  ProjectDetailsCell.swift
//  BWorld
//
//  Created by Kassim Denim on 10/02/2022.
//

import UIKit

class ProjectDetailsCell: UITableViewCell {

    @IBOutlet weak var sliderView:SliderView!
    @IBOutlet weak var lblTitle:UILabel!
    @IBOutlet weak var lblDescription:UILabel!
    @IBOutlet weak var lblPollCount: UILabel!
    @IBOutlet weak var lblVotesDown: UILabel!
    @IBOutlet weak var lblVotesUp: UILabel!
    var data:ResponseProjectDetail?{
        didSet {
            sliderView.images = data?.status == 1 ? (self.data?.returnURlThumbnails() ?? []):[]
            self.lblTitle.text = self.data?.projecttitle
            self.lblDescription.text = self.data?.projectdescription
            self.lblVotesUp.text = self.data?.votesup?.toString
            self.lblVotesDown.text = self.data?.votesdown?.toString
            self.lblPollCount.text = self.data?.returnPollingCount()
            
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
