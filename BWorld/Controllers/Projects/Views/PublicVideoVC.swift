//
//  PublicVideoVC.swift
//  BWorld
//
//  Created by Kassim Denim on 11/02/2022.
//

import UIKit
import CoreAudio

class PublicVideoVC: TabbarControllersBaseView {
   
    @IBOutlet weak var tableView:UITableView!{
        didSet{
            self.tableView.registerTVC(OtherProjectDetailCell.self)
            self.tableView.delegate = self
            self.tableView.dataSource = self
        }
    }
    
    var viewModel = ProjectsViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.getDummyRequest()
        viewModel.getPulblicProjects()
        viewModel.handlerSuccess = {
            self.tableView.reloadData()
            
        }
        // Do any additional setup after loading the view.
    }

}

extension PublicVideoVC:UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.publicProjects?.docs?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: OtherProjectDetailCell.identifier, for: indexPath) as! OtherProjectDetailCell
        let data = self.viewModel.publicProjects?.docs?[indexPath.row]
        cell.sliderView.images = data?.returnURlThumbnails() ?? []
        cell.data = data
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = MediaPlayViewController.getVC(.mediaPlay)
        vc.projectsList = self.viewModel.publicProjects?.docs ?? []
        vc.selectedIndex = indexPath.row
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
