//
//  OtherProjectDetailCell.swift
//  BWorld
//
//  Created by Kassim Denim on 14/02/2022.
//

import UIKit

class OtherProjectDetailCell: UITableViewCell {

    
    @IBOutlet weak var sliderView:SliderView!
    @IBOutlet weak var lblTitle:UILabel!
    @IBOutlet weak var lblDescription:UILabel!
    @IBOutlet weak var lblUser:UILabel!
    @IBOutlet weak var lblVotesUp: UILabel!
    @IBOutlet weak var lblVotesDown: UILabel!
    @IBOutlet weak var lblPollCount: UILabel!
    var data:ResponseProjectDetail?{
        didSet {
            debugPrint(data?.email)
            sliderView.images = data?.status == 1 ? (self.data?.returnURlThumbnails() ?? []):[]
            self.lblTitle.text = self.data?.projecttitle
            self.lblDescription.text = self.data?.projectdescription
            self.lblUser.text = self.data?.email
            self.lblVotesUp.text = self.data?.votesup?.toString
            self.lblVotesDown.text = self.data?.votesdown?.toString
             self.lblPollCount.text = self.data?.returnPollingCount()
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func increment(number1: Int, number2: Int) -> String {
      let finalnum = (number1 + number2)
        return String(finalnum)
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
